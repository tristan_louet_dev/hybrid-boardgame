﻿using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;

namespace Assets.Scripts.GameStates
{
    public class PlayerTurnSkillState : PlayersTurnState
    {
        /// <summary>
        /// Id of the skill.
        /// </summary>
        private readonly int _skillId;

        /// <summary>
        /// Wether it is a passive skill or not.
        /// </summary>
        private readonly bool _passive;

        public override string Name
        {
            get { return base.Name + ", Player " + mPlayerTurn + ", UnitManager " + _skillId + ", Passive ? " + _passive; }
        }

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="previousState">The previous active state.</param>
        /// <param name="skillId">Id of the skill.</param>
        /// <param name="passive">Wether it is a passive skill or not.</param>
        public PlayerTurnSkillState(PlayersTurnState previousState, int skillId, bool passive) : base(previousState)
        {
            _skillId = skillId;
            _passive = passive;
        }

        public override void OnActivated()
        {
            base.OnActivated();
            EventManager.mOnDice += OnDice;
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();
            EventManager.mOnDice -= OnDice;
        }

        /// <summary>
        /// Called when the dice has been rolled.
        /// </summary>
        private void OnDice(int value)
        {
            GameManager.Instance.State = new PlayerTurnFinishedState(this);
        }
    }
}
