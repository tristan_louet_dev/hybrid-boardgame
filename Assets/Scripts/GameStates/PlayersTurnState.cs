﻿using System.Collections.Generic;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.GameStates;

namespace Assets.Scripts.GameStates
{
    /// <summary>
    /// State for when its the turn of players to play.
    /// </summary>
    public abstract class PlayersTurnState : StateBase
    {
        /// <summary>
        /// The characters that did not play their turn yet.
        /// </summary>
        protected readonly HashSet<Character> mRemainingToPlayCharacters = new HashSet<Character>();

        /// <summary>
        /// Id of current player.
        /// </summary>
        protected readonly Character mPlayerTurn;

        public override string Name
        {
            get { return base.Name + " " + mPlayerTurn; }
        }

        public override Character Player
        {
            get { return mPlayerTurn; }
        }

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="playerTurn">Current player.</param>
        /// <param name="remainingCharacters">The remaining characters to play</param>
        protected PlayersTurnState(Character playerTurn, HashSet<Character> remainingCharacters)
        {
            mRemainingToPlayCharacters = remainingCharacters;
            mPlayerTurn = playerTurn;
        }

        protected PlayersTurnState(PlayersTurnState previouState)
        {
            mRemainingToPlayCharacters = previouState.mRemainingToPlayCharacters;
            mPlayerTurn = previouState.mPlayerTurn;
        }

        public override void OnUpdated()
        {
        }
    }
}
