﻿using Assets.commons.Scripts.GameStates;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.GameStates
{
    /// <summary>
    /// Game state for victory.
    /// </summary>
    public class VictoryState : StateBase {

        public override void OnActivated()
        {
            base.OnActivated();
            NetworkManager.Instance.NetworkAPI.UpdateGameState(GameState.Victory);
        }

        public override void OnUpdated()
        {
        }

    }
}
