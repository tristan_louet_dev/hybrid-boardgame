﻿using System.Linq;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;

namespace Assets.Scripts.GameStates
{
    public class PlayerTurnFinishedState : PlayersTurnState {
        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="previousState">The previous active PlayersTurnState.</param>
        public PlayerTurnFinishedState(PlayersTurnState previousState) : base(previousState)
        {}

        public override void OnActivated()
        {
            base.OnActivated();
            // Notify.
            EventManager.mOnPlayerTurnFinished.Fire(mPlayerTurn);
            // Check what to do next.
            if (ObjectiveManager.Instance.IsComplete())
            {
                // Objective completed.
                GameManager.Instance.State = new VictoryState();
            }
            else
            {
                UnityEngine.Debug.Log("Remaining characters count = " + mRemainingToPlayCharacters.Count);
                switch (mRemainingToPlayCharacters.Count)
                {
                    case 0:
                        // todo tick Players debuff
                        GameManager.Instance.State = new AITurnState();
                        break;
                    case 1:
                    {
                        var newCharacter = mRemainingToPlayCharacters.ToArray()[0];
                        GameManager.Instance.State = new PlayerTurnStartingState(newCharacter, mRemainingToPlayCharacters);
                    }
                        break;
                    default:
                        GameManager.Instance.State = new ChoosePlayerOrderState(mRemainingToPlayCharacters);
                        break;
                }
            }
        }
    }
}
