﻿using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;

namespace Assets.Scripts.GameStates
{
    /// <summary>
    /// Game state for when the player is in interaction mode.
    /// </summary>
    public class PlayerTurnInteractState : PlayersTurnState
    {
        /// <summary>
        /// If the player can interact with the tile he is looking at.
        /// </summary>
        private bool _canInteract;

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="previousState">the previous PlayersTurnState that was active</param>
        public PlayerTurnInteractState(PlayersTurnState previousState)
            : base(previousState)
        {}

        public override void OnActivated()
        {
            base.OnActivated();
            EventManager.mOnActionConfirm += OnActionConfirm;
            EventManager.mOnActionCancel += OnActionCancel;
            EventManager.mOnPlayerInteracting.Fire(true);
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();
            EventManager.mOnActionConfirm -= OnActionConfirm;
            EventManager.mOnActionCancel -= OnActionCancel;
        }

        /// <summary>
        /// Check if the player can interact with something.
        /// </summary>
        public override void OnUpdated()
        {
            base.OnUpdated();
            // Check if the player can interact with something.
            if (PlayerManager.Instance.GetPlayer(mPlayerTurn).CanInteract())
            {
                if (_canInteract) return;
                _canInteract = true;
                EventManager.mOnPlayerCanInteract.Fire(true);
            }
            else if (_canInteract)
            {
                _canInteract = false;
                EventManager.mOnPlayerCanInteract.Fire(false);
            }
        }

        /// <summary>
        /// Confirm interaction.
        /// </summary>
        private void OnActionConfirm()
        {
            if (PlayerManager.Instance.GetPlayer(mPlayerTurn).Interact())
            {
                GameManager.Instance.State = new PlayerTurnActionFinishedState(this);
            }
        }

        /// <summary>
        /// Cancel interaction.
        /// </summary>
        private void OnActionCancel()
        {
            EventManager.mOnPlayerInteracting.Fire(false);
            GameManager.Instance.State = new PlayerTurnState(this);
        }
    }
}
