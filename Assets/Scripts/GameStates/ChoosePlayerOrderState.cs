﻿using System.Collections.Generic;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.GameStates;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;
using Assets.Scripts.Screen;

namespace Assets.Scripts.GameStates
{
    /// <summary>
    /// State for when players choose the order in which they will play.
    /// </summary>
    public class ChoosePlayerOrderState : StateBase
    {
        private readonly HashSet<Character> _remainingCharactersToPlay;

        public ChoosePlayerOrderState(HashSet<Character> remainingCharactersToPlay)
        {
            _remainingCharactersToPlay = remainingCharactersToPlay;
        }

        public override void OnActivated()
        {
            base.OnActivated();
            EventManager.mOnActionPlayerOrderChosen += OnActionPlayerOrderChosen;
            EventManager.mOnChoosePlayersOrder.Fire();
            GameManager.Instance.TurnCount++;  // TODO to remove.
            NetworkManager.Instance.NetworkAPI.UpdateTurnCount(GameManager.Instance.TurnCount);
            NetworkManager.Instance.NetworkAPI.UpdateGameState(GameState.PlayerOrderChoice);
            NetworkManager.Instance.NetworkAPI.RequestNextPlayer(new CharactersSet(_remainingCharactersToPlay));
        }

        /// <summary>
        /// Called when players chosen in which order they will play.
        /// </summary>
        /// <param name="chosenCharacter">The chosen player to play.</param>
        private void OnActionPlayerOrderChosen(Character chosenCharacter)
        {
            NetworkManager.Instance.NetworkAPI.UpdateTurnCount(GameManager.Instance.TurnCount); // TODO to remove.
            NetworkManager.Instance.NetworkAPI.UpdateObjective(ObjectiveManager.Instance.Objective.ToString()); // TODO to remove.
            if (!_remainingCharactersToPlay.Contains(chosenCharacter))
            {
                UnityEngine.Debug.LogError("Character " + chosenCharacter + " is not in the remainingCharactersToPlay set");
            }
            GameManager.Instance.State = new PlayerTurnStartingState(chosenCharacter, _remainingCharactersToPlay);
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();
            EventManager.mOnActionPlayerOrderChosen -= OnActionPlayerOrderChosen;
        }

        public override void OnUpdated()
        {
            //GridDrawer.Instance.UncolorizeAll();
            foreach (var player in PlayerManager.Instance.Players)
            {
                GridDrawer.Instance.Colorize(player.Tile, player.Color);
            }
        }
    }
}
