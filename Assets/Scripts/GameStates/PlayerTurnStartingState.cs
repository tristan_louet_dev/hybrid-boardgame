﻿using System.Collections.Generic;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;

namespace Assets.Scripts.GameStates
{
    /// <summary>
    /// Game state for when the player turn is starting.
    /// </summary>
    public class PlayerTurnStartingState : PlayersTurnState
    {
        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="currentCharacter">The character starting its turn.</param>
        /// <param name="otherRemainingCharacters">The remaining characters to play, without the currentCharacter.</param>
        public PlayerTurnStartingState(Character currentCharacter, HashSet<Character> otherRemainingCharacters)
            : base(currentCharacter, otherRemainingCharacters)
        {}

        /// <summary>
        /// Check if the player must spawn or not.
        /// </summary>
        public override void OnActivated()
        {
            base.OnActivated();

            var player = PlayerManager.Instance.GetPlayer(mPlayerTurn);
            mRemainingToPlayCharacters.Remove(mPlayerTurn);
            player.APCurrent = player.APMax;
            if (!player.Spawned)
            {
                GameManager.Instance.State = new PlayerSpawnState(player, new PlayerTurnState(this, true));
            }
            else
            {
                GameManager.Instance.State = new PlayerTurnState(this, true);
            }
        }
    }
}
