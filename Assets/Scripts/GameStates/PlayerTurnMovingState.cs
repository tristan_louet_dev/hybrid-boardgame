﻿using System.Collections;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;
using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Units;
using Assets.Scripts.Utils;

namespace Assets.Scripts.GameStates
{
    /// <summary>
    /// Game state for when the player is moving.
    /// </summary>
    public class PlayerTurnMovingState : PlayersTurnState
    {
        /// <summary>
        /// Paths to follow.
        /// </summary>
        private readonly List<List<Vector2>> _paths;

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="previousState">The previous active PlayersTurnState.</param>
        /// <param name="paths">Paths to follow.</param>
        public PlayerTurnMovingState(PlayersTurnState previousState, List<List<Vector2>> paths)
            : base(previousState)
        {
            _paths = paths;
        }

        public override void OnActivated()
        {
            base.OnActivated();
            GameManager.Instance.StartCoroutine(Move());
        }

        private IEnumerator Move()
        {
            var player = PlayerManager.Instance.GetPlayer(mPlayerTurn);
            yield return GameManager.Instance.StartCoroutine(UnitManager.Instance.MoveTo(player, _paths.ToArray()));
            // Notify.
            EventManager.mOnPlayerPositionChanged.Fire(mPlayerTurn);
            // Action finished.
            GameManager.Instance.State = new PlayerTurnActionFinishedState(this);
        }

        private void CheckVictory()
        {
        }
    }
}
