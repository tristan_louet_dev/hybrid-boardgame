﻿using Assets.commons.Scripts.GameStates;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Entity;
using Assets.Scripts.Managers;
using Assets.Scripts.Screen;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.GameStates {
    /// <summary>
    /// A state to spawn a player at its first play turn. This will give a ServerPlayer its initial tile position.
    /// </summary>
    class PlayerSpawnState : StateBase
    {
        private readonly ServerPlayer _toSpawnPlayer;
        private readonly IState _fallbackState;

        public PlayerSpawnState(ServerPlayer toSpawnPlayer, IState fallbackState)
        {
            _toSpawnPlayer = toSpawnPlayer;
            _fallbackState = fallbackState;
        }

        public override void OnActivated()
        {
            base.OnActivated();
            GridDrawer.Instance.Visible = true;
            foreach (var tile in GridManager.Instance.DeployableTiles)
            {
                GridDrawer.Instance.Colorize(tile, new Color(0, 1, 0, 0.5f));
            }

            // On a pawn detection, use it has this player pawn.
            EventManager.mOnPawnGoOnBoard += PawnDetected;
            // Notify.
            NetworkManager.Instance.NetworkAPI.UpdateGameState(GameState.PlayerSpawn, _toSpawnPlayer.Id);
            EventManager.mOnPlayerSpawning.Fire(_toSpawnPlayer.Id);
        }

        private void PawnDetected(Pawn pawn)
        {
            var tile = GridManager.Instance.TileAt(GridManager.Instance.WorldToTile(pawn.transform.position));
            if (tile != null)
            {
                if (GridManager.Instance.DeployableTiles.Contains(tile))
                {
                    Debug.Log("Spawn on " + tile.Position + " accepted with pawn " + pawn.name);
                    _toSpawnPlayer.Spawn(tile, pawn);
                    EventManager.mOnPlayerSpawned.Fire(_toSpawnPlayer.Id);
                    GameManager.Instance.State = _fallbackState;
                    return;
                }
                Debug.Log("Spawn on " + tile.Position + " refused with pawn " + pawn.name);
            }
        }

        public override void OnUpdated()
        {}

        public override void OnDeactivated()
        {
            base.OnDeactivated();
            EventManager.mOnPawnGoOnBoard -= PawnDetected;
            foreach (var tile in GridManager.Instance.DeployableTiles)
            {
                GridDrawer.Instance.Uncolorize(tile);
            }
            GridDrawer.Instance.Visible = false;
        }
    }
}
