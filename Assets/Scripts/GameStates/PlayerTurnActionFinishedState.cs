﻿using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;

namespace Assets.Scripts.GameStates
{
    /// <summary>
    /// Game state for when the player finished an action.
    /// </summary>
    public class PlayerTurnActionFinishedState : PlayersTurnState
    {
        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="previousState">The PlayersTurnState that was active</param>
        public PlayerTurnActionFinishedState(PlayersTurnState previousState)
            : base(previousState)
        {}

        /// <summary>
        /// Check if player can continue its turn or not.
        /// </summary>
        public override void OnActivated()
        {
            base.OnActivated();
            // Check what to do next.
            if (ObjectiveManager.Instance.IsComplete())
            {
                // Objective completed.
                GameManager.Instance.State = new VictoryState();
            }
            else
            {
                // Check if there is remaining action points.
                var player = PlayerManager.Instance.GetPlayer(mPlayerTurn);
                --player.APCurrent;
                if (player.APCurrent <= 0)
                {
                    // Turn finished.
                    GameManager.Instance.State = new PlayerTurnFinishedState(this);
                }
                else
                {
                    // Same player.
                    GameManager.Instance.State = new PlayerTurnState(this);
                }
            }
        }
    }
}
