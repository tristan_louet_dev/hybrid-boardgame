﻿using System.Collections;
using System.Collections.Generic;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.GameStates;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.GameStates
{
    public class AITurnState : StateBase
    {
       
        public override void OnActivated()
        {
            base.OnActivated();
            EventManager.mOnPlayersTurnFinished.Fire();
            EventManager.mOnAITurnStarted.Fire();

            GameManager.Instance.StartCoroutine(ActivateAI());
        }

        IEnumerator ActivateAI()
        {
            Debug.Log("enemies count = " + EnemyManager.Instance.mEnemies.Count);
            foreach (var e in EnemyManager.Instance.mEnemies)
            {
                Debug.Log("enemy = " + e);
                yield return GameManager.Instance.StartCoroutine(e.OneTurn());
            }

            // Start next phase

            Debug.Log("endAI");
            EventManager.mOnGameTurnFinished.Fire();
            EventManager.mOnGameTurnStarted.Fire();
            GameManager.Instance.State = new ChoosePlayerOrderState(new HashSet<Character> { Character.Amy, Character.Dominic, Character.Jake });
        }

      
        public override void OnUpdated()
        {
            
        }



    }
}
