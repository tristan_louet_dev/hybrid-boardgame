﻿using System.Collections.Generic;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;
using Assets.Scripts.Units;
using Assets.Scripts.Utils;

namespace Assets.Scripts.GameStates
{
    public class PlayerTurnState : PlayersTurnState
    {
        /// <summary>
        /// Wether listeners must be notified or not.
        /// </summary>
        private readonly bool _notify;

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="previouState">The previous active PlayersTurnState.</param>
        /// <param name="notifyCompanion">Wether or not the companion should be notified of the game state update. It usually will be false but in some
        /// particular cases can be forced to true.</param>
        public PlayerTurnState(PlayersTurnState previouState, bool notifyCompanion = false) : base(previouState)
        {
            _notify = notifyCompanion;
        }

        public override void OnActivated()
        {
            base.OnActivated();
            EventManager.mOnPawnGoAway += OnPawnGoAway;
            EventManager.mOnActionActiveSkill += OnActionActiveSkill;
            EventManager.mOnActionPassiveSkill += OnActionPassiveSkill;
            EventManager.mOnActionInteract += OnActionInteract;
            // Notify.
            if (_notify)
            {
                EventManager.mOnPlayerTurnStarted.Fire(mPlayerTurn);
            }
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();
            EventManager.mOnPawnGoAway -= OnPawnGoAway;
            EventManager.mOnActionActiveSkill -= OnActionActiveSkill;
            EventManager.mOnActionPassiveSkill -= OnActionPassiveSkill;
            EventManager.mOnActionInteract -= OnActionInteract;
        }

        /// <summary>
        /// Called when a pawn has been removed from the tablet.
        /// </summary>
        private void OnPawnGoAway(Pawn pawn)
        {
            if (PlayerManager.Instance.GetPlayer(mPlayerTurn).Pawn != pawn) return;
            GameManager.Instance.State = new PlayerTurnMoveState(this);
        }

        /// <summary>
        /// Called when a player decides to use an active skill.
        /// </summary>
        private void OnActionActiveSkill(int skillId)
        {
            GameManager.Instance.State = new PlayerTurnSkillState(this, skillId, false);
        }

        /// <summary>
        /// Called when a player decides to use a passive skill.
        /// </summary>
        private void OnActionPassiveSkill(int skillId)
        {
            GameManager.Instance.State = new PlayerTurnSkillState(this, skillId, true);
        }

        /// <summary>
        /// Called when a player decides to interact with the scenery.
        /// </summary>
        private void OnActionInteract()
        {
            GameManager.Instance.State = new PlayerTurnInteractState(this);
        }
    }
}
