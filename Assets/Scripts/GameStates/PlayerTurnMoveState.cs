﻿using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;
using Assets.Scripts.Screen;
using Assets.Scripts.Utils;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.GameStates
{
    public class PlayerTurnMoveState : PlayersTurnState
    {
        /// <summary>
        /// Color to indicate the initial player position during move.
        /// </summary>
        private static readonly Color POSITION_COLOR = new Color(1, 0.64f, 0, 0.5f);

        /// <summary>
        /// Color to indicate where the player can move.
        /// </summary>
        private static readonly Color MOVE_COLOR = new Color(0, 1, 0, 0.5f);

        /// <summary>
        /// Color to indicate the waypoints.
        /// </summary>
        private static readonly Color WAYPOINT_COLOR = new Color(0.5f, 0, 0.5f, 0.5f);

        /// <summary>
        /// Initial player tile coordinates.
        /// </summary>
        private Vector2 _playerTile;

        /// <summary>
        /// Current player tile coordinates.
        /// </summary>
        private Vector2 _movePlayerTile;

        /// <summary>
        /// Remaining player speed.
        /// </summary>
        private int _remainingSpeed;

        /// <summary>
        /// Wether the pawn is moving or not.
        /// </summary>
        private bool _moving = true;

        /// <summary>
        /// Waypoints.
        /// </summary>
        private List<Vector2> _waypoints = new List<Vector2>(); 

        /// <summary>
        /// Paths.
        /// </summary>
        private readonly List<List<Vector2>> _paths = new List<List<Vector2>>();

        public override string Name
        {
            get { return base.Name + ", moving ? " + (_paths.Count > 0); }
        }

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="previousState">The previous active PlayersTurnState.</param>
        public PlayerTurnMoveState(PlayersTurnState previousState) : base(previousState)
        {}

        public override void OnActivated()
        {
            base.OnActivated();
            EventManager.mOnPawnGoAway += OnPawnGoAway;
            EventManager.mOnPawnGoOnBoard += OnPawnGoOnBoard;
            EventManager.mOnActionConfirm += OnActionConfirm;
            EventManager.mOnActionCancel += OnActionCancel;
            // Notify.
            EventManager.mOnPlayerMoving.Fire(true);
            // Compute the accessibility map the first time.
            var player = PlayerManager.Instance.GetPlayer(mPlayerTurn);
            _playerTile = player.Tile;
            _movePlayerTile = _playerTile;
            _remainingSpeed = player.MovementMax;
            ComputePath();
            // Show the grid.
            GridDrawer.Instance.Visible = true;
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();
            EventManager.mOnPawnGoAway -= OnPawnGoAway;
            EventManager.mOnPawnGoOnBoard -= OnPawnGoOnBoard;
            EventManager.mOnActionConfirm -= OnActionConfirm;
            EventManager.mOnActionCancel -= OnActionCancel;
        }

        /// <summary>
        /// Called when a pawn has been removed from the tablet.
        /// </summary>
        private void OnPawnGoAway(Pawn pawn)
        {
            if (_moving || PlayerManager.Instance.GetPlayer(mPlayerTurn).Pawn != pawn) return;
            _moving = true;
            EventManager.mOnPlayerCanMove.Fire(false);
        }

        /// <summary>
        /// Called when a pawn has been placed on the tablet.
        /// </summary>
        private void OnPawnGoOnBoard(Pawn pawn)
        {
            if (!_moving || PlayerManager.Instance.GetPlayer(mPlayerTurn).Pawn != pawn) return;
            _moving = false;
            // Check.
            var pawnTile = GridManager.Instance.WorldToTile(pawn.transform.position);
            if (GridManager.Tile.Equal(_playerTile, pawnTile))
            {
                // Cancel move.
                UncolorizeTiles();
                EventManager.mOnPlayerMoving.Fire(false);
                GameManager.Instance.State = new PlayerTurnState(this);
            }
            else if (_remainingSpeed > 0 && !GridManager.Tile.Equal(_movePlayerTile, pawnTile) && AccessibilityMap.Instance.TilesAvailablesHero.Contains(pawnTile))
            {
                // Accessible tile, validate the movement.
                _movePlayerTile = pawnTile;
                var path = AccessibilityMap.Instance.GetPath(pawnTile);
                _paths.Add(path);
                _waypoints.Add(pawnTile);
                // Decrement the remaining speed.
                _remainingSpeed -= (path.Count - 1);
                // Compute the new path.
                ComputePath();
                EventManager.mOnPlayerCanMove.Fire(true);
            }
            else if (GridManager.Tile.Equal(_movePlayerTile, pawnTile))
            {
                EventManager.mOnPlayerCanMove.Fire(true);
            }
        }

        /// <summary>
        /// Called when the player decides to validate its move.
        /// </summary>
        private void OnActionConfirm()
        {
            if (_paths.Count > 0)
            {
                // Hide the grid.
                GridDrawer.Instance.Visible = false;
                UncolorizeTiles();
                // Make the player move.
                GameManager.Instance.State = new PlayerTurnMovingState(this, _paths);
            }
            else
            {
                Debug.Log("Invalid move."); // TODO tell it to tablet
            }
        }

        /// <summary>
        /// Called when the player cancelled its move.
        /// </summary>
        private void OnActionCancel()
        {
            GridDrawer.Instance.Visible = false;
            UncolorizeTiles();
            EventManager.mOnPlayerMoving.Fire(false);
            GameManager.Instance.State = new PlayerTurnState(this);
        }

        /// <summary>
        /// Uncolorize the tiles.
        /// </summary>
        private void UncolorizeTiles()
        {
            GridDrawer.Instance.Uncolorize(POSITION_COLOR);
            GridDrawer.Instance.Uncolorize(MOVE_COLOR);
            GridDrawer.Instance.Uncolorize(WAYPOINT_COLOR);
        }

        /// <summary>
        /// Compute the path for the player position and remaining speed.
        /// </summary>
        private void ComputePath()
        {
            UncolorizeTiles();
            // Walkable tiles.
            var player = PlayerManager.Instance.GetPlayer(mPlayerTurn);
            AccessibilityMap.Instance.ComputePath(player, _movePlayerTile, _remainingSpeed);
            foreach (var tile in AccessibilityMap.Instance.TilesAvailablesHero)
            {
                GridDrawer.Instance.Colorize(tile, MOVE_COLOR);
            }
            // Waypoints.
            foreach (var tile in _waypoints)
            {
                GridDrawer.Instance.Colorize(tile, WAYPOINT_COLOR);
            }
            // Initial tile.
            GridDrawer.Instance.Colorize(_playerTile, POSITION_COLOR);
        }
    }
}
