﻿using System.Collections.Generic;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.Network;
using Assets.commons.Scripts.GameStates;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Network
{
    public class ServerNetworkAPI : NetworkAPIBase, INetworkAPI
    {
        #region RPCCall

        [RPC]
        protected override void ServerCallRPC(byte[] values)
        {
            if (NetworkManager.Instance.IsCompanionConnected)
            {
                networkView.RPC("ServerCallRPC", RPCMode.Others, values);
            }
        }

        [RPC]
        protected override void ClientCallRPC(byte[] values)
        {
            string method;
            var list = Deserialize(values, out method);
            GetType().GetMethod(method).Invoke(this, list);
        }

        #endregion

        public void UpdateTurnCount(int value)
        {
            ServerCallRPCSerialize(value);
        }

        public void UpdateObjective(string value)
        {
            ServerCallRPCSerialize(value);
        }

        public void UpdateGameState(GameState state, Character player = Character.None)
        {
            ServerCallRPCSerialize(state, player);
        }

        public void RequestNextPlayer(CharactersSet remainingCharactersToPlay)
        {
            ServerCallRPCSerialize(remainingCharactersToPlay);
        }

        public void UpdateLife(Character player, float value)
        {
            ServerCallRPCSerialize(player, value);
        }

        public void PlayerMoving(bool value)
        {
            ServerCallRPCSerialize(value);
        }

        public void PlayerCanMove(bool value)
        {
            ServerCallRPCSerialize(value);
        }

        public void PlayerInteracting(bool value)
        {
            ServerCallRPCSerialize(value);
        }

        public void PlayerCanInteract(bool value)
        {
            ServerCallRPCSerialize(value);
        }

        public void CompanionReady()
        {
            EventManager.mOnCompanionReady.Fire();
        }

        public void ActionPlayerOrderChosen(Character character)
        {
            EventManager.mOnActionPlayerOrderChosen.Fire(character);
        }

        public void ActionActiveSkill(int skillId)
        {
            EventManager.mOnActionActiveSkill.Fire(skillId);
        }

        public void ActionPassiveSkill(int skillId)
        {
            EventManager.mOnActionPassiveSkill.Fire(skillId);
        }

        public void ActionInteract()
        {
            EventManager.mOnActionInteract.Fire();
        }

        public void ActionConfirm()
        {
            EventManager.mOnActionConfirm.Fire();
        }

        public void ActionCancel()
        {
            EventManager.mOnActionCancel.Fire();
        }
    }
}
