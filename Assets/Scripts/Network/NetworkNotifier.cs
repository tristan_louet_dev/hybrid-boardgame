﻿using Assets.commons.Scripts;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.GameStates;
using Assets.Scripts.Managers;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Network
{
    public class NetworkNotifier : Singleton<NetworkNotifier>
    {
        public override void Awake ()
        {
            base.Awake();
            EventManager.mOnChoosePlayersOrder += OnChoosePlayerOrder;
            EventManager.mOnPlayerTurnStarted += OnPlayerTurnStarted;
            EventManager.mOnAITurnStarted += OnAITurnStarted;
            EventManager.mOnPlayerMoving += OnPlayerMoving;
            EventManager.mOnPlayerCanMove += OnPlayerCanMove;
            EventManager.mOnPlayerInteracting += OnPlayerInteracting;
            EventManager.mOnPlayerCanInteract += OnPlayerCanInteract;
        }

        /// <summary>
        /// Called when players must choose in which order they will play.
        /// </summary>
        private static void OnChoosePlayerOrder()
        {
            NetworkManager.Instance.NetworkAPI.UpdateGameState(GameState.PlayerOrderChoice);
        }

        /// <summary>
        /// Called when a player started its turn.
        /// </summary>
        /// <param name="id">Player id.</param>
        private static void OnPlayerTurnStarted(Character id)
        {
            NetworkManager.Instance.NetworkAPI.UpdateGameState(GameState.PlayerTurn, id);
        }

        /// <summary>
        /// Called when its the turn of AI.
        /// </summary>
        private static void OnAITurnStarted()
        {
            NetworkManager.Instance.NetworkAPI.UpdateGameState(GameState.AITurn);
        }

        /// <summary>
        /// Called when the player is moving.
        /// </summary>
        /// <param name="value">Value.</param>
        private static void OnPlayerMoving(bool value)
        {
            NetworkManager.Instance.NetworkAPI.PlayerMoving(value);
        }

        /// <summary>
        /// Called when the player can validate a movement.
        /// </summary>
        /// <param name="value">Value.</param>
        private static void OnPlayerCanMove(bool value)
        {
            NetworkManager.Instance.NetworkAPI.PlayerCanMove(value);
        }

        /// <summary>
        /// Called when the player is searching for interaction.
        /// </summary>
        /// <param name="value">Value.</param>
        private static void OnPlayerInteracting(bool value)
        {
            NetworkManager.Instance.NetworkAPI.PlayerInteracting(value);
        }

        /// <summary>
        /// Called when the player can interact with the tile he is facing.
        /// </summary>
        /// <param name="value">Value.</param>
        private static void OnPlayerCanInteract(bool value)
        {
            NetworkManager.Instance.NetworkAPI.PlayerCanInteract(value);
        }
    }
}
