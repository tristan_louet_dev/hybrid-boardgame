﻿using Assets.Scripts.Managers;
using Assets.Scripts.Screen;
using UnityEditor;

[CustomEditor(typeof(GridManager))]
public class GridManagerEditor : Editor
{
    private PropertyField[] _fields;
    private GridManager _instance;

    public void OnEnable()
    {
        _instance = target as GridManager;
        _fields = ExposeProperties.GetProperties(_instance);
    }

    public override void OnInspectorGUI()
    {
        if (_instance == null)
            return;

        DrawDefaultInspector();

        ExposeProperties.Expose(_fields);
    }
}