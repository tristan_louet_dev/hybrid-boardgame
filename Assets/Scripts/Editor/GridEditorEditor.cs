﻿using Assets.Scripts.Managers;
using UnityEditor;

[CustomEditor(typeof(GridEditor))]
public class GridEditorEditor : Editor
{
    private PropertyField[] _fields;
    private GridEditor _instance;

    public void OnEnable()
    {
        _instance = target as GridEditor;
        _fields = ExposeProperties.GetProperties(_instance);
    }

    public override void OnInspectorGUI()
    {
        if (_instance == null)
            return;

        DrawDefaultInspector();

        ExposeProperties.Expose(_fields);
    }
}