﻿using Assets.Scripts.Utils.TilesModifiers;
using UnityEditor;

[CustomEditor(typeof(Interactable))]
public class InteractableEditor : Editor
{
    private PropertyField[] _fields;
    private Interactable _instance;

    public void OnEnable()
    {
        _instance = target as Interactable;
        _fields = ExposeProperties.GetProperties(_instance);
    }

    public override void OnInspectorGUI()
    {
        if (_instance == null)
            return;

        DrawDefaultInspector();

        ExposeProperties.Expose(_fields);
    }
}