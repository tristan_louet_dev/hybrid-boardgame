﻿using Assets.Scripts.Utils.TilesModifiers;
using UnityEditor;

namespace Assets.Scripts.Editor
{
    [CustomEditor(typeof(TileModifierWalkability))]
    public class TileModifierWalkabilityEditor : UnityEditor.Editor
    {
        private PropertyField[] _fields;
        private TileModifierWalkability _instance;

        public void OnEnable()
        {
            _instance = target as TileModifierWalkability;
            _fields = ExposeProperties.GetProperties(_instance);
        }

        public override void OnInspectorGUI()
        {
            if (_instance == null)
                return;

            DrawDefaultInspector();

            ExposeProperties.Expose(_fields);
        }
    }
}