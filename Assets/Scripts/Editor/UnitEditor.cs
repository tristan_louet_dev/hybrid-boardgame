﻿using Assets.commons.Scripts.Entity;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Editor
{

    [CustomEditor(typeof(Unit),true)]
    public class UnitEditor : UnityEditor.Editor
    {
        private PropertyField[] _fields;
        private Unit _instance;

        public void OnEnable()
        {
            _instance = target as Unit;
            _fields = ExposeProperties.GetProperties(_instance);
        }

        public override void OnInspectorGUI()
        {
            if (_instance == null)
                return;

            DrawDefaultInspector();

            ExposeProperties.Expose(_fields);
        }
    }
}
