﻿using Assets.Scripts.Screen;
using UnityEditor;

[CustomEditor(typeof (GridDrawer))]
public class GridDrawerEditor : Editor
{
    private PropertyField[] _fields;
    private GridDrawer _instance;

    public void OnEnable()
    {
        _instance = target as GridDrawer;
        _fields = ExposeProperties.GetProperties(_instance);
    }

    public override void OnInspectorGUI()
    {
        if (_instance == null)
            return;

        DrawDefaultInspector();

        ExposeProperties.Expose(_fields);
    }
}