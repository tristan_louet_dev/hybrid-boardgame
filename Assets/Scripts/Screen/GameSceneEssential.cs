﻿using UnityEngine;

namespace Assets.Scripts.Screen
{
    [ExecuteInEditMode]
    public class GameSceneEssential : MonoBehaviour {

#if UNITY_EDITOR
        void Awake()
        {
            Update();
        }

        void Start()
        {
            Update();
        }

        void Update () {
            if (Application.isPlaying) return;
            transform.position = Vector3.zero;
            Camera.main.transform.position = new Vector3(0, 0, -10);
        }
#endif
    }
}
