﻿using Assets.commons.Scripts;
using Assets.Scripts.Managers;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Screen
{
    [ExecuteInEditMode]
    public class ScreenResolution : Singleton<ScreenResolution>
    {

        /// <summary>
        /// Expected screen width.
        /// </summary>
        public const int EXPECTED_WIDTH = 2560;

        /// <summary>
        /// Expected screen height.
        /// </summary>
        public const int EXPECTED_HEIGHT = 1600;

        /// <summary>
        /// Expected screen aspect ratio.
        /// </summary>
        public const float EXPECTED_ASPECT_RATIO = 1.6f;

        /// <summary>
        /// Real screen width.
        /// </summary>
        public static int Width { get; private set; }

        /// <summary>
        /// Real screen height.
        /// </summary>
        public static int Height { get; private set; }

        /// <summary>
        /// Real screen aspect ratio.
        /// </summary>
        public static float AspectRatio { get; private set; }

        /// <summary>
        /// Screen ratio on the x-axis.
        /// </summary>
        public static float RatioX { get; private set; }

        /// <summary>
        /// Screen ratio on the y-axis.
        /// </summary>
        public static float RatioY { get; private set; }

        /// <summary>
        /// Bounds of the game area displayed on the screen (without letterbox/pillarbox).
        /// </summary>
        public static Rect GameBounds { get; private set; }

        /// <summary>
        /// Bounds of the game area displayed on the screen in world coordinates.
        /// </summary>
        public static Rect WorldBounds { get; private set; }

        /// <summary>
        /// Update screen resolution.
        /// </summary>
        public override void Awake()
        {
            base.Awake();
            UpdateResolution();
            UpdateBounds();
        }

        /// <summary>
        /// Update screen resolution.
        /// </summary>
        private void Update()
        {
            if (Width != UnityEngine.Screen.width || Height != UnityEngine.Screen.height)
            {
                UpdateResolution();
            }
            UpdateBounds();
        }

        /// <summary>
        /// Update screen resolution.
        /// </summary>
        private static void UpdateResolution()
        {
            Width = UnityEngine.Screen.width;
            Height = UnityEngine.Screen.height;
            AspectRatio = Height/(float)Width;
            RatioX = Width / (float)EXPECTED_WIDTH;
            RatioY = Height / (float)EXPECTED_HEIGHT;
            // Current viewport height should be scaled by this amount.
            var scaleheight = (Width/(float)Height) / EXPECTED_ASPECT_RATIO;
            var camera = Camera.main;
            var bounds = new Rect(0, 0, Width, Height);
            if (scaleheight < 1.0f)
            {
                // Add letterbox.
                var rect = camera.rect;
                rect.width = 1.0f;
                rect.height = scaleheight;
                rect.x = 0;
                rect.y = (1.0f - scaleheight) / 2.0f;
                camera.rect = rect;
                bounds.y = Height*rect.y;
                bounds.height = Height*scaleheight;
            }
            else
            {
                // Add pillarbox.
                var scalewidth = 1.0f / scaleheight;
                var rect = camera.rect;
                rect.width = scalewidth;
                rect.height = 1.0f;
                rect.x = (1.0f - scalewidth) / 2.0f;
                rect.y = 0;
                camera.rect = rect;
                bounds.x = Width*rect.x;
                bounds.width = Width*scalewidth;
            }
            GameBounds = bounds;
        }

        /// <summary>
        /// Update the bounds of the game in world coordinates.
        /// </summary>
        private static void UpdateBounds()
        {
            // Update the bounds in world coordinates.
            var pos = Camera.main.transform.position;
            var bounds = new Rect { };
            bounds.height = Camera.main.orthographicSize * 2.0f;
            bounds.width = (bounds.height / (float)GameBounds.height) * GameBounds.width;
            bounds.x = -bounds.width / 2.0f + pos.x;
            bounds.y = -bounds.height / 2.0f + pos.y;
            WorldBounds = bounds;
            // Notify listeners.
            EventManager.mOnScreenResolutionChanged.Fire();
        }

        #region DEBUG
#if UNITY_EDITOR
        /// <summary>
        /// Wether the game area bounds are displayed.
        /// </summary>
        public bool mDisplayBounds;

        /// <summary>
        /// White 2D texture.
        /// </summary>
        private Texture2D _whiteTexture;

        /// <summary>
        /// Debug informations.
        /// </summary>
        private void OnGUI()
        {
            if (!Debug.isDebugBuild) return;
            var x = 10 + GameBounds.x;
            GUI.Label(new Rect(x, GameBounds.y, 300, 20), UnityEngine.Screen.width + "x" + UnityEngine.Screen.height + ": " + AspectRatio);
            GUI.Label(new Rect(x, 20 + GameBounds.y, 300, 20), EXPECTED_WIDTH + "x" + EXPECTED_HEIGHT + ": " + EXPECTED_ASPECT_RATIO);
            GUI.Label(new Rect(x, 40 + GameBounds.y, 300, 20), Camera.main.aspect + " " + Camera.main.orthographicSize);
            // Display the bounds.
            if (!mDisplayBounds) return;
            GUI.Label(new Rect(x, 60 + GameBounds.y, UnityEngine.Screen.width - 10, 20),
                "Game Bounds{ x: " + GameBounds.x + ", y: " + GameBounds.y + ", w: " + GameBounds.width + ", h: " + GameBounds.height + "}");
            GUI.Label(new Rect(x, 80 + GameBounds.y, UnityEngine.Screen.width - 10, 20),
                "World Bounds{ x: " + WorldBounds.x + ", y: " + WorldBounds.y + ", w: " + WorldBounds.width + ", h: " + WorldBounds.height + "}");
            if (_whiteTexture == null)
            {
                _whiteTexture  = new Texture2D(1, 1);
                _whiteTexture.SetPixel(0, 0, Color.white);
                _whiteTexture.Apply();
            }
            var oldColor = GUI.color;
            GUI.color = Color.green;
            GUI.DrawTexture(new Rect(GameBounds.x, GameBounds.y - 1, GameBounds.width, 2), _whiteTexture);
            GUI.DrawTexture(new Rect(GameBounds.x, GameBounds.y - 1 + GameBounds.height, GameBounds.width, 2), _whiteTexture);
            GUI.DrawTexture(new Rect(GameBounds.x - 1, GameBounds.y, 2, GameBounds.height), _whiteTexture);
            GUI.DrawTexture(new Rect(GameBounds.x - 1 + GameBounds.width, GameBounds.y, 2, GameBounds.height), _whiteTexture);
            GUI.color = oldColor;
        }
#endif
        #endregion
    }
}
