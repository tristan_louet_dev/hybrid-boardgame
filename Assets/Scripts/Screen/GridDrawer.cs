﻿using System.Collections.Generic;
using Assets.commons.Scripts;
using Assets.Scripts.Managers;
using Assets.Scripts.Utils;
using Microsoft.Win32.SafeHandles;
using UnityEngine;

namespace Assets.Scripts.Screen
{
    /// <summary>
    /// Component drawing the 2D grid.
    /// </summary>
    [ExecuteInEditMode]
    public class GridDrawer : Singleton<GridDrawer>
    {
        /// <summary>
        /// White 2D texture.
        /// </summary>
        private static Texture2D _whiteTexture;

        /// <summary>
        /// Create a 1x1 2D texture of given color.
        /// </summary>
        /// <param name="color">Color to fill the texture with.</param>
        /// <returns>Created texture.</returns>
        private static Texture2D CreateTexture(Color color)
        {
            var texture = new Texture2D(1, 1);
            texture.SetPixel(0, 0, color);
            texture.Apply();
            return texture;
        }

        /// <summary>
        /// Half width of grid lines.
        /// </summary>
        private float _halfLineWidth;

        /// <summary>
        /// Width of grid lines.
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private float _lineWidth;

        /// <summary>
        /// Get/Set width of grid lines.
        /// </summary>
        [ExposeProperty]
        public float LineWidth
        {
            get { return _lineWidth;  }
            set
            {
                _halfLineWidth = _lineWidth/2.0f;
                _lineWidth = value;
            }
        }

        /// <summary>
        /// Color for grid lines.
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private Color _lineColor;

        /// <summary>
        /// Get/Set the color for grid lines.
        /// </summary>
        [ExposeProperty]
        public Color LineColor
        {
            get { return _lineColor; }
            set { _lineColor = value; }
        }

        /// <summary>
        /// Wether border lines are drawn or not.
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private bool _borderLinesDrawn;

        /// <summary>
        /// Get/Set wether the border lines are drawn or not.
        /// </summary>
        [ExposeProperty]
        public bool BorderLinesDrawn
        {
            get { return _borderLinesDrawn; }
            set { _borderLinesDrawn = value; }
        }

        /// <summary>
        /// Wether the grid is visible or not.
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private bool _visible;

        /// <summary>
        /// Get/Set wether the grid is visible or not.
        /// </summary>
        [ExposeProperty]
        public bool Visible
        {
            get { return _visible; }
            set { _visible = value; }
        }

        /// <summary>
        /// List of colored tiles by position.
        /// </summary>
        private readonly Dictionary<Vector2, Color> _tileToColor = new Dictionary<Vector2, Color>();

        /// <summary>
        /// List of colored tiles by color.
        /// </summary>
        private readonly Dictionary<Color, List<Vector2>> _colorToTile = new Dictionary<Color, List<Vector2>>();


        private readonly Dictionary<Color, List<Vector2>> _colorToLineVertical = new Dictionary<Color, List<Vector2>>();

        private readonly Dictionary<Color, List<Vector2>> _colorToLineHorizontal = new Dictionary<Color, List<Vector2>>();

        /// <summary>
        /// Colorize the tile at the given position.
        /// </summary>
        /// <param name="position">Tile coordinates.</param>
        /// <param name="color">Color.</param>
        public void Colorize(Vector2 position, Color color)
        {
            // Position to tile.
            if (_tileToColor.ContainsKey(position))
            {
                RemoveColorToTile(position);
                _tileToColor[position] = color;
            }
            else
            {
                _tileToColor.Add(position, color);
            }
            // Color to tile.
            if (_colorToTile.ContainsKey(color))
            {
                _colorToTile[color].Add(position);
            }
            else
            {
                var list = new List<Vector2> { position };
                _colorToTile.Add(color, list);
            }
        }


        public void ColorizeLineVertical(Vector2 position, Color color)
        {


            /* Position to tile.
            if (_tileToColor.ContainsKey(position))
            {
                RemoveColorToTile(position);
                _tileToColor[position] = color;
            }
            else
            {
                _tileToColor.Add(position, color);
            }*/
            // Color to tile.
            if (_colorToLineVertical.ContainsKey(color))
            {
                _colorToLineVertical[color].Add(position);
            }
            else
            {
                var list = new List<Vector2> { position };
                _colorToLineVertical.Add(color, list);
            }
        }

        public void ColorizeLineHorizontal(Vector2 position, Color color)
        {
            /*
            Position to tile.
            if (_tileToColor.ContainsKey(position))
            {
                RemoveColorToTile(position);
                _tileToColor[position] = color;
            }
            else
            {
                _tileToColor.Add(position, color);
            }
            */
            // Color to tile.
            if (_colorToLineHorizontal.ContainsKey(color))
            {
                _colorToLineHorizontal[color].Add(position);
            }
            else
            {
                var list = new List<Vector2> { position };
                _colorToLineHorizontal.Add(color, list);
            }
        }


        /// <summary>
        /// Colorize the tile at the given position.
        /// </summary>
        /// <param name="tile">The tile to color.</param>
        /// <param name="color">Color.</param>
        public void Colorize(GridManager.Tile tile, Color color)
        {
            Colorize(tile.Position, color);
        }

        /// <summary>
        /// Uncolorize the tile at the given position.
        /// </summary>
        /// <param name="position">Tile coordinates.</param>
        public void Uncolorize(Vector2 position)
        {
            if (!_tileToColor.ContainsKey(position)) return;
            RemoveColorToTile(position);
            _tileToColor.Remove(position);
        }

        /// <summary>
        /// Uncolorize the tile at the given position.
        /// <param name="tile">The tile to uncolorize.</param>
        /// </summary>
        public void Uncolorize(GridManager.Tile tile)
        {
            Uncolorize(tile.Position);
        }

        /// <summary>
        /// Uncolorize all the tiles with the given color.
        /// </summary>
        /// <param name="color">Color of tiles to uncolorize</param>
        public void Uncolorize(Color color)
        {
            if (!_colorToTile.ContainsKey(color)) return;
            foreach (var position in _colorToTile[color])
            {
                _tileToColor.Remove(position);
            }
            _colorToTile.Remove(color);
        }

        /// <summary>
        /// Uncolorize all the tiles.
        /// </summary>
        public void UncolorizeAll()
        {
            _tileToColor.Clear();
            _colorToTile.Clear();
            _colorToLineVertical.Clear();
            _colorToLineHorizontal.Clear();
        }

        /// <summary>
        /// Remove the mapped color from _colorToTile for the given position.
        /// </summary>
        /// <param name="position">Tile coordinates.</param>
        private void RemoveColorToTile(Vector2 position)
        {
            var oldColor = _tileToColor[position];
            _colorToTile[oldColor].Remove(position);
            if (_colorToTile[oldColor].Count == 0)
            {
                _colorToTile.Remove(oldColor);
            }
        }

        /// <summary>
        /// Draw the grid.
        /// </summary>
        private void OnGUI()
        {
            if (!Visible) return;
            if (_whiteTexture == null)
            {
                _whiteTexture = CreateTexture(Color.white);
            }
            // Draw the grid.
            var oldColor = GUI.color;
            DrawColoredTiles();
            DrawGridLines();
            GUI.color = oldColor;
        }

        /// <summary>
        /// Draw the colored tiles.
        /// </summary>
        private void DrawColoredTiles()
        {
            var height = GridManager.Instance.Height;
            var gameTileSize = GridManager.Instance.GameTileSize;
            var bounds = new Rect(0, 0, gameTileSize.x, gameTileSize.y);
            foreach (var entry in _colorToTile)
            {
                GUI.color = entry.Key;
                foreach (var position in entry.Value)
                {
                    bounds.x = gameTileSize.x * position.x + ScreenResolution.GameBounds.x;
                    bounds.y = gameTileSize.y * (height - position.y - 1) + ScreenResolution.GameBounds.y;
                    GUI.DrawTexture(bounds, _whiteTexture);
                }
            }
        }

        /// <summary>
        /// Draw the grid lines.
        /// </summary>
        private void DrawGridLines()
        {
            var width = GridManager.Instance.Width;
            var height = GridManager.Instance.Height;
            var gameTileSize = GridManager.Instance.GameTileSize;
            // Draw vertical lines.
            GUI.color = _lineColor;
            var line = new Rect(-_halfLineWidth - gameTileSize.x + ScreenResolution.GameBounds.x, ScreenResolution.GameBounds.y, _lineWidth, ScreenResolution.GameBounds.height);
            for (var i = 0; i <= width; ++i)
            {
                line.x += gameTileSize.x;
                if (!_borderLinesDrawn && (i == 0 || i == width))
                {
                    continue;
                }
                GUI.DrawTexture(line, _whiteTexture);
            }
            // Draw horizontal lines.
            line = new Rect(ScreenResolution.GameBounds.x, -_halfLineWidth - gameTileSize.y + ScreenResolution.GameBounds.y, ScreenResolution.GameBounds.width, _lineWidth);
            for (var j = 0; j <= height; ++j)
            {
                line.y += gameTileSize.y;
                if (!_borderLinesDrawn && (j == 0 || j == height))
                {
                    continue;
                }
                GUI.DrawTexture(line, _whiteTexture);
            }
        }

        #region GIZMOS
#if UNITY_EDITOR
        /// <summary>
        /// Draw the grid lines for editor.
        /// </summary>
        private void OnDrawGizmos()
        {
            var oldColor = Gizmos.color;
            
            DrawGizmosGridLines();
            DrawGizmosLinesColor();
            DrawGizmosColoredTiles();

            Gizmos.color = oldColor;
        }

        /// <summary>
        /// Draw the colored tiles.
        /// </summary>
        private void DrawGizmosColoredTiles()
        {
            var pos = Camera.main.transform.position;
            var halfWorldScreenWidth = ScreenResolution.WorldBounds.width / 2.0f;
            var halfWorldScreenHeight = ScreenResolution.WorldBounds.height / 2.0f;
            var size = new Vector3(GridManager.Instance.WorldTileSize.x, GridManager.Instance.WorldTileSize.y, 0.1f);
            var center = new Vector3(0, 0, 10);
            foreach (var entry in _colorToTile)
            {
                Gizmos.color = entry.Key;
                foreach (var position in entry.Value)
                {
                    center.x = size.x * (position.x + 0.5f) - halfWorldScreenWidth + pos.x;
                    center.y = size.y * (position.y + 0.5f) - halfWorldScreenHeight + pos.y;
                    Gizmos.DrawCube(center, size);
                }
            }
        }

       

        /*
         * Useless, for now
        private void DrawGridLinesColor()
        {
            var gameTileSize = GridManager.Instance.GameTileSize;

            // Draw vertical lines.
            var line = new Rect(-_halfLineWidth - gameTileSize.x + ScreenResolution.GameBounds.x, ScreenResolution.GameBounds.y, _lineWidth * 4, gameTileSize.y);
            foreach (var entry in _colorToLineVertical)
            {

                GUI.color = entry.Key;
                foreach (var position in entry.Value)
                {
                    line.x = -_halfLineWidth - gameTileSize.x + ScreenResolution.GameBounds.x;
                    line.x += gameTileSize.x * (position.x + 1); // + ScreenResolution.GameBounds.x;
                    line.y = (GridManager.Instance.Height - position.y - 1) * gameTileSize.y;

                    GUI.DrawTexture(line, _whiteTexture);
                }
            }

            // Draw horizontal lines.
            line = new Rect(ScreenResolution.GameBounds.x, -_halfLineWidth - gameTileSize.y + ScreenResolution.GameBounds.y, gameTileSize.x, _lineWidth * 4);
            foreach (var entry in _colorToLineHorizontal)
            {

                GUI.color = entry.Key;
                foreach (var position in entry.Value)
                {
                    line.x = ScreenResolution.GameBounds.x;
                    line.x += gameTileSize.x * (position.x); // + ScreenResolution.GameBounds.x;

                    line.y = -_halfLineWidth - gameTileSize.y + ScreenResolution.GameBounds.y;

                    line.y += (GridManager.Instance.Height - position.y + 1) * gameTileSize.y;

                    GUI.DrawTexture(line, _whiteTexture);
                }
            }



        }*/


        /// <summary>
        /// Draw the grid lines for editor.
        /// </summary>
        private void DrawGizmosGridLines()
        {
            var width = GridManager.Instance.Width;
            var height = GridManager.Instance.Height;
            var worldTileSize = GridManager.Instance.WorldTileSize;
            // Draw vertical lines.
            Gizmos.color = _lineColor;
            var pos = Camera.main.transform.position;
            var halfWorldScreenWidth = ScreenResolution.WorldBounds.width / 2.0f;
            var halfWorldScreenHeight = ScreenResolution.WorldBounds.height / 2.0f;
            var from = new Vector3(-halfWorldScreenWidth + pos.x, -halfWorldScreenHeight + pos.y, 0);
            var to = new Vector3(-halfWorldScreenWidth + pos.x, halfWorldScreenHeight + pos.y, 0);
            for (var i = 0; i <= width; ++i)
            {
                Gizmos.DrawLine(from, to);
                from.x += worldTileSize.x;
                to.x = from.x;
            }


            // Draw horizontal lines.
            from = new Vector3(-halfWorldScreenWidth + pos.x, -halfWorldScreenHeight + pos.y, 0);
            to = new Vector3(halfWorldScreenWidth + pos.x, -halfWorldScreenHeight + pos.y, 0);
            for (var j = 0; j <= height; ++j)
            {
                Gizmos.DrawLine(from, to);
                from.y += worldTileSize.y;
                to.y = from.y;
            }
        }

        private void DrawGizmosLinesColor()
        {
            var pos = Camera.main.transform.position;
            var halfWorldScreenWidth = ScreenResolution.WorldBounds.width / 2.0f;
            var halfWorldScreenHeight = ScreenResolution.WorldBounds.height / 2.0f;
            var size = new Vector3(GridManager.Instance.WorldTileSize.x, GridManager.Instance.WorldTileSize.y, 0.1f);
            var center = new Vector3(0, 0, 10);
            foreach (var entry in _colorToLineHorizontal)
            {
                Gizmos.color = entry.Key;
                foreach (var position in entry.Value)
                {
                    center.x = size.x * (position.x + 0.5f ) - halfWorldScreenWidth + pos.x;
                    center.y = size.y * (position.y ) - halfWorldScreenHeight + pos.y;


                    Gizmos.DrawCube(center, new Vector3(size.x, 1, size.z));
                }
            }

            foreach (var entry in _colorToLineVertical)
            {
                Gizmos.color = entry.Key;
                foreach (var position in entry.Value)
                {
                    center.x = size.x * (position.x ) - halfWorldScreenWidth + pos.x;
                    center.y = size.y * (position.y + 0.5f) - halfWorldScreenHeight + pos.y;


                    Gizmos.DrawCube(center, new Vector3(1,size.y,size.z));
                }
            }
        }


#endif
        #endregion
    }
}