﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.commons.Scripts;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.Utils;
using Assets.Scripts.Managers;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Units
{
    public class UnitManager : Singleton<UnitManager> {

        // Useless, for now
        //public void UpKeep(Unit unit)
        //{
        //    unit.APCurrent = unit.APMax;
        //}

        /// <summary>
        /// Move the unit as close as possible to the given to tile
        /// </summary>
        /// <param name="unit">Unit to move.</param>
        /// <param name="to">the targeted tile you try to reach</param>
        /// <param name="success">Callback for when the unit successfully moved to the tile.</param>
        /// <param name="failure">Callback for when the unit failed to move to the tile.</param>
        /// <returns></returns>
        public IEnumerator MoveTo(Unit unit, Vector2 to, CallbackEvent success, CallbackEvent failure)
        {
            Debug.Log("from " + unit.Tile + " to " + to);
            AccessibilityMap.Instance.ComputePath(unit, unit.Tile, 50);
            var path = AccessibilityMap.Instance.GetPath(to);

            Debug.Log("all movements");
            foreach (var p in path)
            {
                Debug.Log(p);
            }

            var currentPathPosition = 1;
            var currentMouvCost = unit.MovementMax;
            while (currentPathPosition < path.Count)
            {
                if (currentMouvCost >= unit.MovementMax)
                {
                    if (unit.APCurrent > 0)
                    {
                        unit.APCurrent--;
                        currentMouvCost = 0;
                    }
                    else
                    {
                        Debug.Log("cant reach " + path[currentPathPosition]);
                        if (failure != null)
                        {
                            failure();
                        }
                        yield break;
                    }
                }

                currentMouvCost++;

                Debug.Log("move to " + path[currentPathPosition]);
                yield return StartCoroutine(MoveTo(unit, path[currentPathPosition]));
                unit.Tile = path[currentPathPosition];

                currentPathPosition++;
            }

            if (success != null)
            {
                success();
            }
        }

        /// <summary>
        /// Make unit move along the given paths.
        /// </summary>
        /// <param name="unit">Unit to move.</param>
        /// <param name="paths">Paths to follow.</param>
        /// <returns></returns>
        public IEnumerator MoveTo(Unit unit, params List<Vector2>[] paths)
        {
            return paths.Select(path => StartCoroutine(MoveTo(unit, path))).GetEnumerator();
        }

        /// <summary>
        /// Make unit move along the given path.
        /// </summary>
        /// <param name="unit">Unit to move.</param>
        /// <param name="path">Path to follow.</param>
        /// <returns></returns>
        public IEnumerator MoveTo(Unit unit, List<Vector2> path)
        {
            return path.Select(tile => StartCoroutine(MoveTo(unit, tile))).GetEnumerator();
        }

        /// <summary>
        /// Make unit move to the given tile.
        /// </summary>
        /// <param name="unit">Unit to move.</param>
        /// <param name="tile">Tile coordinates.</param>
        /// <returns></returns>
        public IEnumerator MoveTo(Unit unit, Vector2 tile)
        {
            float t = 0;
            var posOrigine = unit.transform.position;
            var posDest = GridManager.Instance.TileToWorldMiddle(tile);
            // Make the transition.
            while (t <= 1.0f)
            {
                t += Time.deltaTime;
                unit.transform.position = Vector3.Lerp(posOrigine, posDest, t);
                yield return null;
            }
            // Set the new unit tile.
            unit.Tile = tile;
            // Sleep after the move.
            if (unit.TimeForOneMove > 0)
            {
                yield return new WaitForSeconds(unit.TimeForOneMove);
            }
        }

    }
}
