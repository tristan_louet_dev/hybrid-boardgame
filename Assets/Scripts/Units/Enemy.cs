﻿using System.Collections;
using Assets.commons.Scripts.Entity;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Units
{
    public abstract class Enemy : Unit
    {
        public override Camp Camp
        {
            get { return Camp.Enemy; }
        }

        public override Vector2 Tile
        {
            get { return base.Tile; }
            set
            {
                GridManager.Instance.UnitAt(base.Tile, null);
                base.Tile = value;
                transform.position = GridManager.Instance.TileToWorldMiddle(value);
                GridManager.Instance.UnitAt(value, this);
            }
        }

#if UNITY_EDITOR
        void Update()
        {
            Tile = GridManager.Instance.WorldToTile(transform.position);
        }
#endif


        
      
        void Start()
        {
            EnemyManager.Instance.mEnemies.Add(this);
        }

        public abstract IEnumerator OneTurn();

        /*
        protected enum Skills
        {
            Fire
        }
       

        private IEnumerator UseSkill(Skills skill, bool isDone)
        {
            yield break;
        }

        */

        /* void EvaluateActions( List<Vector2> tiles )
        {
             int[] pointsForEvaluation = new int[tiles.Count];
            for (int i = 0; i < pointsForEvaluation.Length; i++)
            {
                pointsForEvaluation[i] = 0;
            }

            for (int i = 0; i<tiles.Count; i++)
            {
                pointsForEvaluation[i] += EvaluateMoreActionForMovement(tiles[i]);


            } 
        }*/



    }
}
