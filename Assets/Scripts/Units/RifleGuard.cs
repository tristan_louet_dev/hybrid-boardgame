﻿using System.Collections;
using Assets.Scripts.Entity;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Units
{
    public class RifleGuard : Enemy
    {

        public override IEnumerator OneTurn()
        {
            Debug.Log(this + " IA Turn");

            //  _accessibilityMap.ComputePath(GridManager.Instance.WorldToTile(transform.position), _unit.MovementMax * _unit.APMax);

            ServerPlayer[] players = PlayerManager.Instance.Players;

            Vector2 target = players[Random.Range(0, players.Length)].Tile;
            Debug.Log("Targeted tile = " + target);
            yield return StartCoroutine(UnitManager.Instance.MoveTo(this, target, () =>
            {
                // Moved to tile.
            }, () =>
            {
                // Failed to move.
            }));


            //  EvaluateActions(_accessibilityMap.TilesAvailables);

        }
       
    }
}
