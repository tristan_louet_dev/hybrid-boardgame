﻿using System.Collections.Generic;
using Assets.commons.Scripts;
using Assets.commons.Scripts.Entity;
using Assets.Scripts.Screen;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.TilesModifiers;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    /// <summary>
    /// Manager for the game grid.
    /// </summary>
    [ExecuteInEditMode]
    public class GridManager : Singleton<GridManager>
    {
        /// <summary>
        /// Size of one tile from the game area on the screen.
        /// </summary>
        [HideInInspector]
        public Vector2 GameTileSize { get; private set; }

        /// <summary>
        /// Size of one tile from the game area on the screen in world coordinates.
        /// </summary>
        [HideInInspector]
        public Vector2 WorldTileSize { get; private set; }

        /// <summary>
        /// Number of columns.
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private int _width;

        /// <summary>
        /// Get/Set the number of columns.
        /// </summary>
        [ExposeProperty]
        public int Width
        {
            get { return _width; }
            set
            {
                _width = value;
                OnScreenResolutionChanged();
                Init(_width, _height);
            }
        }

        /// <summary>
        /// Number of rows.
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private int _height;

        /// <summary>
        /// Get/Set the number of rows.
        /// </summary>
        [ExposeProperty]
        public int Height
        {
            get { return _height; }
            set
            {
                _height = value;
                OnScreenResolutionChanged();
                Init(_width, _height);
            }
        }

        /// <summary>
        /// Get the grid tiles.
        /// </summary>
        public Tile[,] Tiles { get; private set; }

        public List<Tile> TilesList
        {
            get
            {
                var result = new List<Tile>();
                for (var j = 0; j < _height; ++j)
                {
                    for (var i = 0; i < _width; ++i)
                    {
                        result.Add(Tiles[j, i]);
                    }
                }
                return result;
            }
        }

        public HashSet<Tile> DeployableTiles { get; private set; }

        /// <summary>
        /// Scene root game object.
        /// </summary>
        public GameObject mSceneRoot;

        /// <summary>
        /// Initialize the grid.
        /// </summary>
        /// <param name="width">Width.</param>
        /// <param name="height">Height.</param>
        private void Init(int width, int height)
        {
            // Create the tiles.
            Tiles = new Tile[height, width];
            DeployableTiles = new HashSet<Tile>();
            for (var j = 0; j < height; ++j)
            {
                for (var i = 0; i < width; ++i)
                {
                    Tiles[j, i] = new Tile(i, j);
                }
            }

            // Initia

            // Update the tiles.
            UpdateTiles();
        }

        /// <summary>
        /// Update the tiles properties.
        /// </summary>
        private void UpdateTiles()
        {
#if UNITY_EDITOR
            if (Tiles == null)
            {
                return;
            }
            if (!Application.isPlaying)
            {
                GridDrawer.Instance.UncolorizeAll();
            }
#endif
            // Reset tiles.
            for (var j = 0; j < _height; ++j)
            {
                for (var i = 0; i < _width; ++i)
                {
                    var tile = Tiles[j, i];
                    DeployableTiles.Clear();
                    tile.Init();
                }
            }

            // Check all the game objects with a TileModifier component.
            if (mSceneRoot == null) return;
            foreach (var component in mSceneRoot.transform.GetComponentsInChildren<TileModifier>())
            {
                var bounds = TileBounds(component.gameObject);
                component.Action(bounds);
            }
        }

        /// <summary>
        /// Get the bounds of the given game object as tiles.
        /// </summary>
        /// <param name="oGameObject">Game object</param>
        /// <returns>The bounds.</returns>
        private Rect TileBounds(GameObject oGameObject)
        {
            // Get the bounds.
            var from = oGameObject.transform.position;
            var to = oGameObject.transform.position;
            var oCollider = oGameObject.GetComponent<Collider2D>();
            if (oCollider != null && oCollider.enabled)
            {
                from = oCollider.bounds.min;
                to = oCollider.bounds.max;
            }
            else if (oGameObject.renderer != null)
            {
                var bounds = oGameObject.renderer.bounds;
                from = bounds.min;
                to = bounds.max;
            }
            // Convert to tiles.
            var fromTile = WorldToTile(from);
            var toTile = WorldToTile(to);
            return new Rect(fromTile.x, fromTile.y, toTile.x - fromTile.x + 1, toTile.y - fromTile.y + 1);
        }

        /// <summary>
        /// Indicate it the grid contains the given tile.
        /// </summary>
        /// <param name="tile">Tile coordinates.</param>
        /// <returns>If the grid contains the tile.</returns>
        public bool Contains(Vector2 tile)
        {
            return Tiles != null && tile.x >= 0 && tile.y >= 0 && tile.x < _width && tile.y < _height;
        }

        /*  public bool IsWalkable(Vector2 tile)
          {
              return Contains(tile) && TileAt(tile).mWalkability == TileModifierWalkability.Walkability.Free;
          }*/

        /// <summary>
        /// Indicate if a player at "from" tile can interact with the given "target" tile.
        /// </summary>
        /// <param name="mode">Activation mode.</param>
        /// <param name="from">Tile containing the player.</param>
        /// <param name="target">Tile to interact with.</param>
        /// <returns>If it is interactable.</returns>
        public bool IsInteractable(ActivationMode mode, Vector2 from, Vector2 target)
        {
            // Check if tiles are valids.
            if (!Contains(from) || !Contains(target)) return false;
            // Check if there is an interactable object.
            var tile = TileAt(target);
            return tile.mInteractable != null && tile.mInteractable.IsInteractable(mode, from, target);
        }

        /// <summary>
        /// Interact with the given tile.
        /// </summary>
        /// <param name="mode">Activation mode.</param>
        /// <param name="from">Tile containing the player.</param>
        /// <param name="target">Tile to interact with.</param>
        /// <returns>If it worked.</returns>
        public bool Interact(ActivationMode mode, Vector2 from, Vector2 target)
        {
            if (!IsInteractable(mode, from, target)) return false;
            var t = TileAt(target);
            var result = t.mInteractable.Interact(target);
            // Remove the interactable game object.
            if ((result & InteractionHandler.Result.Remove) == InteractionHandler.Result.Remove)
            {
                Destroy(t.mInteractable.gameObject);
                t.mInteractable = null;
            }
            return true;
        }

        /// <summary>
        /// Indicate if the given tile is a block.
        /// </summary>
        /// <param name="tile">Tile coordinates.</param>
        /// <returns>If it is a block.</returns>
        public bool IsBlock(Vector2 tile)
        {
            return Contains(tile) && TileAt(tile).IsBlock;
        }

        /// <summary>
        /// Indicate if the given tile is a hole.
        /// </summary>
        /// <param name="tile">Tile coordinates.</param>
        /// <returns>If it is a hole.</returns>
        public bool IsHole(Vector2 tile)
        {
            return Contains(tile) && TileAt(tile).mIsHole;
        }

        /// <summary>
        /// Get the tile at the given coordinates.
        /// </summary>
        /// <param name="tile">Tile coordinates.</param>
        /// <returns>The tile.</returns>
        public Tile TileAt(Vector2 tile)
        {
            return Contains(tile) ? Tiles[(int)tile.y, (int)tile.x] : null;
        }

        /// <summary>
        /// Get the unit on the given tile.
        /// </summary>
        /// <param name="tile">Tile coordinates.</param>
        /// <returns>The unit.</returns>
        public Unit UnitAt(Vector2 tile)
        {
            var t = TileAt(tile);
            return t == null ? null : t.Unit;
        }

        /// <summary>
        /// Set the unit at the given tile.
        /// </summary>
        /// <param name="tile">Tile coordinates.</param>
        /// <param name="unit">Unit.</param>
        public void UnitAt(Vector2 tile, Unit unit)
        {
            var t = TileAt(tile);
            if (t == null) return;
            t.Unit = unit;
        }

        /// <summary>
        /// Get the tile for the given world position.
        /// </summary>
        /// <param name="position">World position.</param>
        /// <returns>Corresponding tile.</returns>
        public Vector2 WorldToTile(Vector3 position)
        {
            return new Vector2(
                (int)((position.x - ScreenResolution.WorldBounds.x) / WorldTileSize.x),
                (int)((position.y - ScreenResolution.WorldBounds.y) / WorldTileSize.y)
            );
        }

        /// <summary>
        /// Get the world position for the given tile.
        /// </summary>
        /// <param name="tile">Tile coordinates.</param>
        /// <returns>World position.</returns>
        public Vector3 TileToWorld(Vector2 tile)
        {
            return new Vector3(
                (tile.x * WorldTileSize.x) + ScreenResolution.WorldBounds.x,
                (tile.y * WorldTileSize.y) + ScreenResolution.WorldBounds.y,
                0
            );
        }

        public Vector3 TileToWorldMiddle(Vector2 tile)
        {
            Vector3 pos = TileToWorld(tile);

            pos.x += WorldTileSize.x/2;
            pos.y += WorldTileSize.y/2;
            return pos;

        }


        /// <summary>
        /// Called when the singleton awaken.
        /// </summary>
        private void Start()
        {
            Init(_width, _height);
            EventManager.mOnScreenResolutionChanged += OnScreenResolutionChanged;
            EventManager.mOnMapChanged += OnMapChanged;
        }

        void Awake()
        {
            OnScreenResolutionChanged();
           
        }

        private void OnDestroy()
        {
            EventManager.mOnScreenResolutionChanged -= OnScreenResolutionChanged;
            EventManager.mOnMapChanged -= OnMapChanged;
        }

        /// <summary>
        /// When the map changed, update it.
        /// </summary>
        private void OnMapChanged()
        {
            UpdateTiles();
        }

#if UNITY_EDITOR
        /// <summary>
        /// Update the tiles.
        /// </summary>
        private void Update()
        {
            if (!Application.isEditor) return;
            if (Tiles == null)
            {
                Init(_width, _height);
            }
            UpdateTiles();
        }
#endif

        /// <summary>
        /// Called when the screen resolution changed.
        /// </summary>
        private void OnScreenResolutionChanged()
        {
            GameTileSize = new Vector2(ScreenResolution.GameBounds.width / (float)_width, ScreenResolution.GameBounds.height / (float)_height);
            WorldTileSize = new Vector2(ScreenResolution.WorldBounds.width / (float)_width, ScreenResolution.WorldBounds.height / (float)_height);
        }

        /// <summary>
        /// Structure for the grid tiles.
        /// </summary>
        public class Tile
        {
            /// <summary>
            /// Check if the two given vectors are equal.
            /// </summary>
            /// <param name="v1">First vector.</param>
            /// <param name="v2">Second vector.</param>
            /// <returns>If they are equal.</returns>
            public static bool Equal(Vector2 v1, Vector2 v2)
            {
                return ((int) v1.x == (int) v2.x) && ((int) v1.y == (int) v2.y);
            }

            public Tile(int x, int y)
            {
                Position = new Vector2(x, y);
                Init();

            }


            public void Init()
            {
                mComeFromUp = true;
                mComeFromDown = true;
                mComeFromRight = true;
                mComeFromLeft = true;

                mIsHole = false;

                mCostToComeIn = 1;

                mInteractable = null;
                AllowDeployment = false;
                IsExit = false;
                ExitID = 0;
            }

            /// <summary>
            /// Walkability of the tile.
            /// </summary>
            public bool mComeFromUp;
            public bool mComeFromDown;
            public bool mComeFromRight;
            public bool mComeFromLeft;

            public bool mIsHole;

            public int mCostToComeIn;

            /// <summary>
            /// Get/Set the unit on this tile.
            /// </summary>
            public Unit Unit { get; set; }

            /// <summary>
            /// Passability of the tile.
            /// </summary>
            //     public TileModifierSpeed.Passability mPassability = TileModifierSpeed.Passability.All;

            /// <summary>
            /// Interactable game object on the tile.
            /// </summary>
            public Interactable mInteractable;

            /// <summary>
            /// Indicates if a player can be deployed on this tile at zone start.
            /// </summary>
            public bool AllowDeployment { get; set; }

            /// <summary>
            /// Indicates if a player can exit the map through this tile.
            /// </summary>
            public bool IsExit { get; set; }

            /// <summary>
            /// The ExitID links differents exit tiles together as a single exit zone.
            /// </summary>
            public int ExitID { get; set; }

            /// <summary>
            /// The position of this tile in the grid.
            /// </summary>
            public Vector2 Position { get; set; }

            /// <summary>
            /// The X position of this tile in the grid.
            /// </summary>
            public int X { get { return (int)Position.x; } }
            /// <summary>
            /// The Y position of this tile in the grid.
            /// </summary>
            public int Y { get { return (int)Position.y; } }

            /// <summary>
            /// Get wether the tile is a block or not.
            /// </summary>
            public bool IsBlock
            {
                get { return !mComeFromDown && !mComeFromLeft && !mComeFromRight && !mComeFromUp; }
            }
        }
    }
}
