﻿using Assets.commons.Scripts;
using Assets.commons.Scripts.Network;
using Assets.Scripts.Network;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class NetworkManager : Singleton<NetworkManager>
    {
        #region Networking parameters & properties
        [SerializeField]
        private string _gamePublicName;

        public string GamePublicName
        {
            get { return _gamePublicName; }
            set { _gamePublicName = value; }
        }

        [SerializeField]
        private int _serverPort = 42042;

        [ExposeProperty]
        public int ServerPort {
            get { return _serverPort; }
            private set { _serverPort = value; }
        }

        [SerializeField]
        private string _masterServerIP = "127.0.0.1";
        [ExposeProperty]
        public string MasterServerIP {
            get { return _masterServerIP; }
            private set { _masterServerIP = value; }
        }

        [SerializeField]
        private int _masterServerPort = 23466;
        [ExposeProperty]
        public int MasterServerPort {
            get { return _masterServerPort; }
            private set { _masterServerPort = value; }
        }

        [SerializeField]
        private string _facilitatorServerIP = "127.0.0.1";
        [ExposeProperty]
        public string FacilitatorServerIP
        {
            get { return _facilitatorServerIP; }
            private set { _facilitatorServerIP = value; }
        }

        [SerializeField]
        private int _facilitatorServerPort = 50005;
        [ExposeProperty]
        public int FacilitatorServerPort
        {
            get { return _facilitatorServerPort; }
            private set { _facilitatorServerPort = value; }
        }

        private bool _isCompanionConnected = false;
        public bool IsCompanionConnected
        {
            get { return _isCompanionConnected; }
            set
            {
                var old = _isCompanionConnected;
                _isCompanionConnected = value;
                if (_isCompanionConnected && !old)
                {
                    EventManager.OnCompanionConnected.Fire();
                }
                else if (!_isCompanionConnected && old)
                {
                    EventManager.OnCompanionDisconnected.Fire();
                }
            }
        }
        public NetworkPlayer ConnectedCompanion { get; private set; }

        public INetworkAPI NetworkAPI { get; private set; }
        #endregion

        #region MonoBehavior callbacks

        public override void Awake()
        {
            base.Awake();
            NetworkAPI = GetComponent<ServerNetworkAPI>();
            MasterServer.ipAddress = MasterServerIP;
            MasterServer.port = MasterServerPort;
            UnityEngine.Network.natFacilitatorIP = FacilitatorServerIP;
            UnityEngine.Network.natFacilitatorPort = FacilitatorServerPort;
            StartServer();
        }
        #endregion

        #region Networking functions
        private void StartServer()
        {
            UnityEngine.Network.InitializeServer(1, ServerPort, !UnityEngine.Network.HavePublicAddress());
            MasterServer.RegisterHost(GamePublicName, GamePublicName, "Underground server");
        }
        #endregion

        #region Networking callbacks
        void OnServerInitialized()
        {
            Debug.Log("Server initialized and listening on :" + ServerPort + ".");
        }

        void OnMasterServerEvent(MasterServerEvent e)
        {
            switch (e)
            {
                case MasterServerEvent.RegistrationSucceeded:
                    Debug.Log("Server successfully registered.");
                    break;
                default:
                    Debug.LogError("Fatal networking error " + e.ToString() + ".");
                    break;
            }
        }

        void OnPlayerConnected(NetworkPlayer p)
        {
            ConnectedCompanion = p;
            IsCompanionConnected = true;
            Debug.Log("Companion connected - " + p.ipAddress + ":" + p.port);
        }

        void OnPlayerDisconnected(NetworkPlayer p)
        {
            IsCompanionConnected = false;
            Debug.Log("Companion disconnected");
        }
        #endregion
    }
}
