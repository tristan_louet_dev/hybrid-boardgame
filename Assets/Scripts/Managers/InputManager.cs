﻿using System.Collections;
using Assets.commons.Scripts;
using Assets.Scripts.Utils;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Managers
{
    /// <summary>
    /// Keep track of fingers on screen
    /// </summary>
    public class InputManager : Singleton<InputManager>
    {
      /*  // Update is called once per frame
        void Update()
        {
            // First add all fingers
            foreach (var touch in Input.touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        EventManager.mOnAddTouchInput.Fire(touch.fingerId);
                        break;
                    case TouchPhase.Ended:
                    case TouchPhase.Canceled:
                        EventManager.mOnRemoveTouchInput.Fire(touch.fingerId);
                        break;
                   
                }
            }

        }*/

        public HashSet<int> mReservedFingers = new HashSet<int>();

        public Touch[] GetAllUnReservedFingers()
        {
            return (from t in Input.touches let used = mReservedFingers.Any(r => t.fingerId == r) where !used select t).ToArray();
        }
        


        public Touch GetTouchFromFingerId(int fingerId)
        {
            foreach (var t in Input.touches.Where(t => t.fingerId == fingerId))
            {
                return t;
            }
            Debug.LogError(fingerId + " no founded");
            return new Touch();
        }
    }
}
