﻿using Assets.commons.Scripts;
using Assets.Scripts.Objective;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    /// <summary>
    /// Objective manager.
    /// </summary>
    public class ObjectiveManager : Singleton<ObjectiveManager>
    {
        /// <summary>
        /// Objective to complete.
        /// </summary>
        [SerializeField]
        private ObjectiveBase _objective;

        /// <summary>
        /// Get the objective.
        /// </summary>
        public ObjectiveBase Objective
        {
            get { return _objective; }
        }

        /// <summary>
        /// Indicate if the objective is complete.
        /// </summary>
        /// <returns>If the objective is complete.</returns>
        public bool IsComplete()
        {
            return _objective == null || _objective.IsComplete();
        }
    }
}
