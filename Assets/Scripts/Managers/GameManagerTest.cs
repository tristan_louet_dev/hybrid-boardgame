﻿using System.Collections;
using System.Collections.Generic;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.GameStates;
using Assets.Scripts.Screen;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class GameManagerTest : MonoBehaviour
    {
        public bool mAutomatic;

        // Use this for initialization
        void Start ()
        {
            if (mAutomatic)
            {
                EventManager.mOnChoosePlayersOrder += OnChoosePlayersOrder;
                EventManager.mOnPlayerTurnStarted += OnPlayerTurnStarted;
                EventManager.mOnAITurnStarted += OnAITurnStarted;
            }
            
            GameManager.Instance.State = new ChoosePlayerOrderState(new HashSet<Character> { Character.Amy, Character.Dominic, Character.Jake });
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.A) && Input.GetKeyDown(KeyCode.I))
            {
                GameManager.Instance.State = new AITurnState();
            }
        }

        void OnGUI()
        {
            var bounds = ScreenResolution.GameBounds;
            GUI.Label(new Rect(bounds.x + 10, bounds.y + bounds.height - 30, bounds.width, 20), GameManager.Instance.State == null ? "No state" : GameManager.Instance.State.Name);
        }

        private const int WAIT_TIME = 2;

        private void OnChoosePlayersOrder()
        {
            StartCoroutine("TestChoosePlayerOrder");
        }

        //private IEnumerator TestChoosePlayerOrder()
        //{
        //    yield return new WaitForSeconds(WAIT_TIME);
        //    var order = new PlayerOrder();
        //    order.Push(Character.Amy);
        //    order.Push(Character.Dominic);
        //    order.Push(Character.Jake);
        //    EventManager.mOnActionPlayerOrderChosen.Fire(order);
        //}

        private void OnPlayerTurnStarted(Character playerId)
        {
            StartCoroutine("TestPlayerTurn");
        }

        private IEnumerator TestPlayerTurn()
        {
            yield return new WaitForSeconds(WAIT_TIME);
            var val = Random.Range(0, 3);
            switch (val)
            {
                case 0:
                    EventManager.mOnActionActiveSkill.Fire(0);
                    yield return StartCoroutine(TestPlayerTurnSkill(false));
                    break;
                case 1:
                    EventManager.mOnActionPassiveSkill.Fire(0);
                    yield return StartCoroutine(TestPlayerTurnSkill(true));
                    break;
                case 3:
                    EventManager.mOnActionInteract.Fire();
                    yield return StartCoroutine(TestPlayerTurnInteract());
                    break;
            } 
        }

        private static IEnumerator TestPlayerTurnSkill(bool passive)
        {
            yield return new WaitForSeconds(WAIT_TIME);
            EventManager.mOnDice.Fire(0);
        }

        private IEnumerator TestPlayerTurnMove()
        {
            yield return new WaitForSeconds(WAIT_TIME);
            var player = PlayerManager.Instance.GetPlayer(GameManager.Instance.State.Player);
            EventManager.mOnPawnGoAway.Fire(player.Pawn);
            yield return new WaitForSeconds(WAIT_TIME);
            EventManager.mOnPawnGoOnBoard.Fire(player.Pawn);
            yield return new WaitForSeconds(WAIT_TIME);
            yield return StartCoroutine(TestPlayerTurn());
        }

        private IEnumerator TestPlayerTurnInteract()
        {
            yield return new WaitForSeconds(WAIT_TIME);
            EventManager.mOnActionCancel.Fire();
            yield return StartCoroutine(TestPlayerTurn());
        }

        private void OnAITurnStarted()
        {
            StartCoroutine("TestAITurn");
        }

        private IEnumerator TestAITurn()
        {
            yield return new WaitForSeconds(WAIT_TIME);
            EventManager.mOnAITurnFinished.Fire();
            EventManager.mOnGameTurnFinished.Fire();
            EventManager.mOnGameTurnStarted.Fire();
            GameManager.Instance.State = new ChoosePlayerOrderState(new HashSet<Character> { Character.Amy, Character.Dominic, Character.Jake });
        }

    }
}
