﻿using System.Collections.Generic;
using System.Linq;
using Assets.commons.Scripts;
using Assets.commons.Scripts.Entity;
using Assets.Scripts.Units;


namespace Assets.Scripts.Managers
{
    public class EnemyManager : Singleton<EnemyManager>
    {
        public List<Enemy> mEnemies = new List<Enemy>();
    }
}
