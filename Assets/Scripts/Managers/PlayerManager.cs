﻿using System.Collections.Generic;
using System.Linq;
using Assets.commons.Scripts;
using Assets.commons.Scripts.Entity;
using Assets.Scripts.Entity;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class PlayerManager : Singleton<PlayerManager> {
        /// <summary>
        /// List of players.
        /// </summary>
        [SerializeField]
        private ServerPlayer[] _playersList;

        /// <summary>
        /// Players by id.
        /// </summary>
        private readonly Dictionary<Character, ServerPlayer> _players = new Dictionary<Character, ServerPlayer>();

        /// <summary>
        /// Players by pawn.
        /// </summary>
        private readonly Dictionary<Pawn, ServerPlayer> _pawnToplayer = new Dictionary<Pawn, ServerPlayer>();

        /// <summary>
        /// Get the players.
        /// </summary>
        public ServerPlayer[] Players
        {
            get { return _players.Values.ToArray(); }
        }

        /// <summary>
        /// Get the player with the given id.
        /// </summary>
        /// <param name="player">Player id.</param>
        /// <returns>The player.</returns>
        public ServerPlayer GetPlayer(Character player)
        {
            return _players.ContainsKey(player) ? _players[player] : null;
        }

        /// <summary>
        /// Get the player for the given pawn.
        /// </summary>
        /// <param name="pawn">Pawn.</param>
        /// <returns>The player.</returns>
        public ServerPlayer GetPlayer(Pawn pawn)
        {
            return _pawnToplayer.ContainsKey(pawn) ? _pawnToplayer[pawn] : null;
        }

        public override void Awake()
        {
            base.Awake();
            // Set up the dictionary for players.
            foreach (var player in _playersList)
            {
                _players[player.Id] = player;
            }
            EventManager.onPlayerSpawned += (player) => _pawnToplayer[player.Pawn] = player;
        }
    }
}
