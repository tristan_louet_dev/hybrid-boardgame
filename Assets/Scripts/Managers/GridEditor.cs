﻿using Assets.commons.Scripts;
using Assets.Scripts.Screen;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    /// <summary>
    /// Helper for modifying the grid in editor.
    /// </summary>
    [ExecuteInEditMode]
    public class GridEditor : Singleton<GridEditor>
    {
        /// <summary>
        /// Parent for scene assets.
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private GameObject _sceneAssets;

        /// <summary>
        /// Get/Set the parent for scene assets.
        /// </summary>
        [ExposeProperty]
        public GameObject SceneAssets
        {
            get { return _sceneAssets; }
            set
            {
                _sceneAssets = value;
                Init();
            }
        }

        /// <summary>
        /// Asset for the ground.
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private GameObject _groundAsset;

        /// <summary>
        /// Get/Set the ground asset.
        /// </summary>
        [ExposeProperty]
        public GameObject GroundAsset
        {
            get { return _groundAsset; }
            set
            {
                _groundAsset = value;
                InitGround();
            }
        }

        /// <summary>
        /// Asset for the blocks.
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private GameObject _blockAsset;

        /// <summary>
        /// Get/Set the blocks asset.
        /// </summary>
        [ExposeProperty]
        public GameObject BlockAsset
        {
            get { return _blockAsset; }
            set
            {
                _blockAsset = value;
                InitBlocks();
            }
        }

        /// <summary>
        /// Rebuild the scene.
        /// </summary>
        [ExposeProperty]
        public bool Rebuild
        {
            get { return false; }
            set { Init(); }
        }

        /// <summary>
        /// Init the scene.
        /// </summary>
        private void Start()
        {
            Init();
        }

        /// <summary>
        /// Initialize the scene.
        /// </summary>
        private void Init()
        {
            if (_sceneAssets == null) return;
            _sceneAssets.transform.localPosition = new Vector3(0, 0, 10);
            InitGround();
            InitBlocks();
            InitHoles();
        }

        /// <summary>
        /// Initialize the ground.
        /// </summary>
        private void InitGround()
        {
            if (_sceneAssets == null) return;
            // Get the container for automatic ground.
            var transform = FindOrCreate("Ground Auto", 2);
            // No ground asset.
            if (_groundAsset == null) return;
            // Build the ground.
            var pos = Camera.main.transform.position;
            var halfWorldScreenWidth = ScreenResolution.WorldBounds.width / 2.0f;
            var halfWorldScreenHeight = ScreenResolution.WorldBounds.height / 2.0f;
            var size = GridManager.Instance.WorldTileSize;
            for (var j = 0; j < GridManager.Instance.Height; ++j)
            {
                var column = new GameObject("Row " + j);
                column.transform.parent = transform;
                column.transform.rotation = Quaternion.identity;
                column.transform.localPosition = Vector3.zero;
                for (var i = 0; i < GridManager.Instance.Width; ++i)
                {
                    var ground = (GameObject) Instantiate(_groundAsset);
                    ground.transform.parent = column.transform;
                    ground.transform.name = "Ground " + i + "-" + j;
                    ground.transform.localPosition = new Vector3(size.x * (i + 1) - halfWorldScreenWidth + pos.x, size.y * (j + 1) - halfWorldScreenHeight + pos.y, 0);
                    ground.transform.rotation = Quaternion.Euler(-90, 0, 0);
                    ground.transform.localScale = new Vector3(size.x, 1, size.y);
                }
            }
        }

        /// <summary>
        /// Initialize the blocks.
        /// </summary>
        private void InitBlocks()
        {
            if (_sceneAssets == null) return;
            // Get the container for blocks.
            var transform = FindOrCreate("Blocks Auto", 0);
            // No blocks asset.
            if (_blockAsset == null) return;
            // Build the blocks.
            var pos = Camera.main.transform.position;
            var halfWorldScreenWidth = ScreenResolution.WorldBounds.width / 2.0f;
            var halfWorldScreenHeight = ScreenResolution.WorldBounds.height / 2.0f;
            var size = GridManager.Instance.WorldTileSize;
            for (var j = 0; j < GridManager.Instance.Height; ++j)
            {
                var column = new GameObject("Row " + j);
                column.transform.parent = transform;
                column.transform.rotation = Quaternion.identity;
                column.transform.localPosition = Vector3.zero;
                for (var i = 0; i < GridManager.Instance.Width; ++i)
                {
                    if (!GridManager.Instance.IsBlock(new Vector2(i, j))) continue;
                    var block = (GameObject) Instantiate(_blockAsset);
                    block.transform.parent = column.transform;
                    block.transform.localPosition = new Vector3(size.x * (i + 1) - halfWorldScreenWidth + pos.x,
                        size.y*(j + 1) - halfWorldScreenHeight + pos.y, 0);
                    block.transform.rotation = Quaternion.Euler(-90, 0, 0);
                    block.transform.localScale = new Vector3(size.x, 1, size.y);
                }
            }
        }

        /// <summary>
        /// Initialize the holes.
        /// </summary>
        private void InitHoles()
        {
            if (_sceneAssets == null) return;
            var parent = _sceneAssets.transform.FindChild("Ground Auto");
            if (parent == null) return;
            for (var j = 0; j < GridManager.Instance.Height; ++j)
            {
                var column = parent.FindChild("Row " + j);
                if (column == null) continue;
                for (var i = 0; i < GridManager.Instance.Width; ++i)
                {
                    if (!GridManager.Instance.IsHole(new Vector2(i, j))) continue;
                    var transform = column.FindChild("Ground " + i + "-" + j);
                    if (transform != null)
                    {
                        DestroyImmediate(transform.gameObject);
                    }
                }
            }
        }

        /// <summary>
        /// Find or create the game object with the given name.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="depth">Z-depth.</param>
        /// <returns>The game object.</returns>
        private Transform FindOrCreate(string name, int depth)
        {
            var transform = _sceneAssets.transform.FindChild(name);
            if (transform != null)
            {
                DestroyImmediate(transform.gameObject);
            }
            var obj = new GameObject(name);
            transform = obj.transform;
            transform.parent = _sceneAssets.transform;
            transform.rotation = Quaternion.identity;
            transform.localPosition = new Vector3(0, 0, depth);
            return transform;
        }

    }
}