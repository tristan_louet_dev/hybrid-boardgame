﻿using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.Utils;
using Assets.Scripts.Entity;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Managers
{
    /// <summary>
    /// A class managing game events.
    /// <remarks>This class is a Singleton.</remarks>
    /// </summary>
    public class EventManager
    {
        #region ScreenResolution
        /// <summary>
        /// Event called when the screen resolution changed.
        /// </summary>
        public static Event mOnScreenResolutionChanged = new Event();  

        /// <summary>
        /// Event called by InputManager when more fingers are detected
        /// </summary>
        public static Event<int> mOnAddTouchInput = new Event<int>();

        /// <summary>
        /// Event called by InputManager when more fingers are detected
        /// </summary>
        public static Event<int> mOnRemoveTouchInput = new Event<int>();

        /// <summary>
        /// Event called when the given pawn is removed from the board
        /// </summary>
        public static Event<Pawn> mOnPawnGoAway = new Event<Pawn>();

        /// <summary>
        /// Event called when the given pawn is put on board
        /// </summary>
        public static Event<Pawn> mOnPawnGoOnBoard = new Event<Pawn>();
        #endregion

        #region Players
        public static Event<ServerPlayer> onPlayerSpawned = new Event<ServerPlayer>();
        #endregion

        #region GameManager
        /// <summary>
        /// Event called when the companion is ready to receive RPC.
        /// </summary>
        public static Event mOnCompanionReady = new Event();

        /// <summary>
        /// Event called when a new game turn has been started.
        /// </summary>
        public static Event mOnGameTurnStarted = new Event();

        /// <summary>
        /// Event called when players must choose the order in which they will play.
        /// </summary>
        public static Event mOnChoosePlayersOrder = new Event();

        /// <summary>
        /// Event called before players turns. 
        /// </summary>
        public static Event mOnPlayersTurnStarted = new Event();

        /// <summary>
        /// Event called when a player spawns. 
        /// </summary>
        public static Event<Character> mOnPlayerSpawning = new Event<Character>();

        /// <summary>
        /// Event called when a player spawned. 
        /// </summary>
        public static Event<Character> mOnPlayerSpawned = new Event<Character>();

        /// <summary>
        /// Event called when a player started its turn. 
        /// </summary>
        public static Event<Character> mOnPlayerTurnStarted = new Event<Character>();

        /// <summary>
        /// Event called when a player finished its turn. 
        /// </summary>
        public static Event<Character> mOnPlayerTurnFinished = new Event<Character>();

        /// <summary>
        /// Event called after players turns. 
        /// </summary>
        public static Event mOnPlayersTurnFinished= new Event();

        /// <summary>
        /// Event called when the AI started its turn.
        /// </summary>
        public static Event mOnAITurnStarted = new Event();

        /// <summary>
        /// Event called when an enemy started its turn. 
        /// </summary>
        public static Event mOnEnemyTurnStarted = new Event();

        /// <summary>
        /// Event called when an enemy finished its turn. 
        /// </summary>
        public static Event mOnEnemyTurnFinished = new Event();
    
        /// <summary>
        /// Event called when the AI finished its turn.
        /// </summary>
        public static Event mOnAITurnFinished = new Event();

        /// <summary>
        /// Event called when one AI unit finished its turn.
        /// </summary>
        public static Event mOnOneUnitAITurnFinished = new Event();

        /// <summary>
        /// Event called when a game turn has been finished.
        /// </summary>
        public static Event mOnGameTurnFinished = new Event();

        /// <summary>
        /// Event called when a player is moving.
        /// </summary>
        public static Event<bool> mOnPlayerMoving = new Event<bool>();

        /// <summary>
        /// Event called when a player can validate a movement.
        /// </summary>
        public static Event<bool> mOnPlayerCanMove = new Event<bool>();

        /// <summary>
        /// Event called when a player is searching for interaction.
        /// </summary>
        public static Event<bool> mOnPlayerInteracting = new Event<bool>();

        /// <summary>
        /// Event called when a player can interact with the tile he is facing. 
        /// </summary>
        public static Event<bool> mOnPlayerCanInteract = new Event<bool>();

        /// <summary>
        /// Event called when a player position changed.
        /// </summary>
        public static Event<Character> mOnPlayerPositionChanged = new Event<Character>();

        /// <summary>
        /// Event called when the map changed.
        /// </summary>
        public static Event mOnMapChanged = new Event();
        #endregion

        #region GameState
        #region ChoosePlayerOrderState
        /// <summary>
        /// Event called when the next player to play has been chosen.
        /// </summary>
        public static Event<Character> mOnActionPlayerOrderChosen = new Event<Character>();
        #endregion

        #region PlayerTurnState
        /// <summary>
        /// Event called when a player decides to use an active skill.
        /// </summary>
        public static Event<int> mOnActionActiveSkill = new Event<int>();

        /// <summary>
        /// Event called when a player decides to use a passive skill.
        /// </summary>
        public static Event<int> mOnActionPassiveSkill = new Event<int>();

        /// <summary>
        /// Event called when a player decides to interact with the scenery.
        /// </summary>
        public static Event mOnActionInteract = new Event();

        /// <summary>
        /// Event called when a player confirms its action.
        /// </summary>
        public static Event mOnActionConfirm = new Event();

        /// <summary>
        /// Event called when a player decides to cancel its action.
        /// </summary>
        public static Event mOnActionCancel = new Event();
        #endregion
        #endregion

        /// <summary>
        /// Event called when the dice has been rolled.
        /// </summary>
        public static Event<int> mOnDice = new Event<int>();

        #region Networking
        /// <summary>
        /// Event called when the companion device connected to the game.
        /// </summary>
        public static Event OnCompanionConnected = new Event();

        /// <summary>
        /// Event called when the companion device disconnected from the game.
        /// </summary>
        public static Event OnCompanionDisconnected = new Event();
        #endregion
    }
}