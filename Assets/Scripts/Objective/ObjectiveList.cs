﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Objective
{
    /// <summary>
    /// A list of objectives to complete.
    /// </summary>
    public class ObjectiveList : ObjectiveBase
    {
        /// <summary>
        /// Objectives to complete.
        /// </summary>
        [SerializeField]
        private List<ObjectiveBase> _objectives; 

        public override bool IsComplete()
        {
            return _objectives.All(objective => objective.IsComplete());
        }

        public override string ToString()
        {
            var s = "";
            var first = true;
            foreach (var objective in _objectives)
            {
                s = s + (first ? "" : "\n") + objective.ToString();
                first = false;
            }
            return s;
        }
    }
}
