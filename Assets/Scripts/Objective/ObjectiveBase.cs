﻿using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Objective
{
    /// <summary>
    /// Base for the map objective script.
    /// </summary>
    public abstract class ObjectiveBase : MonoBehaviour
    {
        /// <summary>
        /// Indicate if the objective has been completed.
        /// </summary>
        /// <returns>If it has been completed.</returns>
        public abstract bool IsComplete();
    }
}
