﻿using UnityEngine;

namespace Assets.Scripts.Objective
{
    /// <summary>
    /// Compare two objectives with a logical operator.
    /// </summary>
    public class LogicalObjective : ObjectiveBase {

        /// <summary>
        /// Operators.
        /// </summary>
        public enum Operator
        {
            And,
            Or
        }

        /// <summary>
        /// First objective.
        /// </summary>
        [SerializeField]
        private ObjectiveBase _left;

        /// <summary>
        /// Operator.
        /// </summary>
        [SerializeField]
        private Operator _operator;
        
        /// <summary>
        /// Second objective.
        /// </summary>
        [SerializeField]
        private ObjectiveBase _right;

        public override bool IsComplete()
        {
            switch (_operator)
            {
                case Operator.And:
                    return _left.IsComplete() && _right.IsComplete();
                case Operator.Or:
                    return _left.IsComplete() || _right.IsComplete();
            }
            return false;
        }

        public override string ToString()
        {
            var left = _left.ToString();
            var right = _right.ToString();
            switch (_operator)
            {
                case Operator.And:
                    return left + "\nAnd\n" + right;
                case Operator.Or:
                    return left + "\nOr\n" + right;
            }
            return "";
        }
    }
}
