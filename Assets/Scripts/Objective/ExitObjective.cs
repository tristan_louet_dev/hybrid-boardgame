﻿using System.Linq;
using Assets.commons.Scripts.Entity;
using Assets.Scripts.Managers;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.TilesModifiers;
using UnityEngine;

namespace Assets.Scripts.Objective
{
    /// <summary>
    /// Objective is to reach the exit.
    /// </summary>
    public class ExitObjective : ObjectiveBase
    {

        /// <summary>
        /// Number of players who must have reached the exit.
        /// </summary>
        [SerializeField]
        private int count;

        public override bool IsComplete()
        {
            if (count == 0) return true;
            var total = 0;
            // ReSharper disable once UnusedVariable
            foreach (var player in PlayerManager.Instance.Players.Where(player => player.ReachedExit))
            {
                ++total;
                if (total >= count)
                {
                    return true;
                }
            }
            return false;
        }

        private void Awake()
        {
            EventManager.mOnPlayerPositionChanged += OnPlayerPositionChanged;
        }

        private void OnDestroy()
        {
            EventManager.mOnPlayerPositionChanged -= OnPlayerPositionChanged;
        }

        /// <summary>
        /// Check if players unlocked and reached the exit.
        /// </summary>
        private static void OnPlayerPositionChanged (Character id)
        {
            var player = PlayerManager.Instance.GetPlayer(id);
            var tile = GridManager.Instance.TileAt(player.Tile);
            Debug.Log("Player moved " + player.Id + " " + tile.X + " " + tile.Y + " " + tile.ExitID);
            if (ObjectiveTilesManager.Instance.ExitUnlocked)
            {
                Debug.Log("Exit already unlocked");
                player.ReachedExit = tile.IsExit;
            } else if (tile.ExitID > 0)
            {
                Debug.Log("Exit unlocked");
                ObjectiveTilesManager.Instance.ObjectiveZoneReached(tile);
                player.ReachedExit = tile.IsExit;
            }
            else
            {
                Debug.Log("No exit");
                player.ReachedExit = false;
            }
        }

        public override string ToString()
        {
            return count + " player(s) must reach the exit.";
        }
    }
}
