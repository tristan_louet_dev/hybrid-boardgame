﻿using Assets.commons.Scripts.Entity;
using Assets.Scripts.Managers;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.TilesModifiers;
using UnityEngine;

namespace Assets.Scripts.Entity
{
    /// <summary>
    /// Player server-side.
    /// </summary>
    public class ServerPlayer : Player
    {
        /// <summary>
        /// Auto place unit on the good tale
        /// Same code on Enemies
        /// </summary>
        public override Vector2 Tile
        {
            get { return base.Tile; }
            set
            {
                GridManager.Instance.UnitAt(base.Tile, null);
                base.Tile = value;
                transform.position = GridManager.Instance.TileToWorldMiddle(value);
                GridManager.Instance.UnitAt(value, this);
            }
        }

        /// <summary>
        /// Player color.
        /// </summary>
        [SerializeField]
        private Color _color;

        /// <summary>
        /// Get the player color.
        /// </summary>
        [ExposeProperty]
        public Color Color
        {
            get { return _color; }
        }

        /// <summary>
        /// Get the pawn instance.
        /// </summary>
        public Pawn Pawn { get; private set; }

        /// <summary>
        /// Indicates wether or not the player has already spawn on the map.
        /// </summary>
        public bool Spawned { get; private set; }

     
        /// <summary>
        /// Tile the player is looking at.
        /// </summary>
        private Vector2 _lookingAtTile;

        /// <summary>
        /// Get the tile the player is looking at.
        /// </summary>
        public Vector2 LookingAtTile
        {
            get { return _lookingAtTile; }
            private set { _lookingAtTile = value; }
        }

      

        /// <summary>
        /// Init the player.
        /// </summary>
        protected void Start()
        {
           // base.Start();
            Spawned = false;

        }

        /// <summary>
        /// Spawns the player on a tile.
        /// </summary>
        /// <param name="tile">The tile to spawn the player on.</param>
        /// <param name="pawn">The pawn that will stay associated with this player.</param>
        public void Spawn(GridManager.Tile tile, Pawn pawn)
        {
            if (Spawned)
            {
                Debug.LogError("You tried to spawn an already spawned pawn.");
                return;
            }
            Pawn = pawn;
            Tile = tile.Position;
            Spawned = true;
            EventManager.onPlayerSpawned.Fire(this);
        }

        /// <summary>
        /// Update the player.
        /// </summary>
        private void Update()
        {
            if (Pawn == null)
            {
                return;
            }
            //Angle = ((int) Pawn.transform.rotation.eulerAngles.z + 90) % 360;
            var Angle = ((int)Pawn.transform.rotation.eulerAngles.z + 90) % 360;
            var dX = 0;
            var dY = 0;
            if (Angle < 45)
            {
                dX = 1;
            }
            else if (Angle < 135)
            {
                dY = 1;
            }
            else if (Angle < 225)
            {
                dX = -1;
            }
            else if (Angle < 315)
            {
                dY = -1;
            }
            else
            {
                dX = 1;
            }
            _lookingAtTile.x = Tile.x + dX;
            _lookingAtTile.y = Tile.y + dY;


#if UNITY_EDITOR
            Tile = GridManager.Instance.WorldToTile(transform.position);
#endif

        }

     
        /// <summary>
        /// Indicate if the player can interact with the tile he is facing.
        /// </summary>
        /// <returns>If the player can interact with the tile.</returns>
        public bool CanInteract()
        {
            return GridManager.Instance.IsInteractable(ActivationMode.Interaction, Tile, LookingAtTile);
        }

        /// <summary>
        /// Interact with the tile the player is facing if possible.
        /// </summary>
        /// <returns>If it worked.</returns>
        public bool Interact()
        {
            return GridManager.Instance.Interact(ActivationMode.Interaction, Tile, LookingAtTile);
        }
    } 
}
