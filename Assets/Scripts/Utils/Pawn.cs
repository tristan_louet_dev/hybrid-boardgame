﻿//#define IN_REAL_WORLD_SPACE
//#define DEBUG

using System;

using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class Pawn : MonoBehaviour
    {
        #region Utils

        private int FindPawnPointIdByKey(int id)
        {
            for (int i = 0; i < _pawnPoints.Count; i++)
            {
                if (_pawnPoints[i].key == id)
                    return i;
            }
            Debug.LogError("No pawn point founded for " + id);
            return -1;
        }

        private List<Tuple<int, Vector2>> FindDetectedPawn()
        {
            return _pawnPoints.Where(p => p.key != -1).ToList();
        }

        private Vector2 Rotate(Vector2 v, float degrees)
        {
            float sin = Mathf.Sin(degrees*Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees*Mathf.Deg2Rad);

            float tx = v.x;
            float ty = v.y;
            v.x = (cos*tx) - (sin*ty);
            v.y = (sin*tx) + (cos*ty);
            return v;
        }

        private void ReserveTouch(Touch touch)
        {
            InputManager.Instance.mReservedFingers.Add(touch.fingerId);

            for (int i = 0; i < _fronts.Length; i++)
            {
                _fronts[i] = 0;
            }
            _finalFront = -1;

            foreach (var p in _pawnPoints.Where(p => p.key == -1))
            {
                p.key = touch.fingerId;
                _realPawnPointCount++;
                return;
            }
            Debug.LogError("Impossible to reserve this touch " + touch);
        }

        #endregion

#if IN_REAL_WORLD_SPACE
    // To convert Distance in Real World
        private const float REALWORLD_HEIGH_MM = 135;
        private const float REALWORLD_WIDTH_MM = 217;
        private float _facteurHeight;
        private float _facteurWidth;
#endif

        /*
         * Impossible : rapport entre la résolution et la taille réelle de la tablette qu'on ne peu récuperer 
         * private float[] _screenSizeCalibration =
        {
            100,
            100
        };*/

        void Start()
        {
#if IN_REAL_WORLD_SPACE
            _facteurHeight = REALWORLD_HEIGH_MM / UnityEngine.Screen.height;
            _facteurWidth = REALWORLD_WIDTH_MM / UnityEngine.Screen.width;
#endif
            Array.Sort(mDistances);
            _pawnPositionOut = -100 * Vector2.one;

            _pawnPoints.Add(new Tuple<int, Vector2>(-1, _pawnPositionOut));
            _pawnPoints.Add(new Tuple<int, Vector2>(-1, _pawnPositionOut));
            _pawnPoints.Add(new Tuple<int, Vector2>(-1, _pawnPositionOut));

            Vector2 positionInScreenSpace = (_pawnPoints[0].value + _pawnPoints[1].value + _pawnPoints[2].value) / 3;
            transform.position = Camera.main.ScreenToWorldPoint(new Vector3(positionInScreenSpace.x, positionInScreenSpace.y, 1));

            for (int i = 0; i < _fronts.Length; i++)
            {
                _fronts[i] = 0;
            }

            
        }

        /// <summary>
        /// use only for Debug purpose
        /// </summary>
        public bool mIsPawnEnteringBoard = false;

        public bool mIsPawnLeavingBoard = false;

        private Vector2 _pawnPositionOut;

        void Update()
        {
            FindPatern();

            Evaluate();

            if (mIsPawnEnteringBoard)
            {
                EventManager.mOnPawnGoOnBoard.Fire(this);
             
                mIsPawnEnteringBoard = false;
            }
        }

        /// <summary>
        ///  update _pawnPoints status
        /// </summary>
        void LateUpdate()
        {
            foreach (var p in _pawnPoints)
            {
                if (p.key != -1 && (InputManager.Instance.GetTouchFromFingerId(p.key).phase == TouchPhase.Ended ||
                    InputManager.Instance.GetTouchFromFingerId(p.key).phase == TouchPhase.Canceled))
                {
                    InputManager.Instance.mReservedFingers.Remove(p.key);

                    p.key = -1;

                    _realPawnPointCount--;

                    if (_realPawnPointCount == 0)
                    {
                        mIsPawnLeavingBoard = true;
                    }
                }
            }

            if (mIsPawnLeavingBoard)
            {
                // RAZ
                _pawnPoints[0].value = _pawnPositionOut;
                _pawnPoints[1].value = _pawnPositionOut;
                _pawnPoints[2].value = _pawnPositionOut;

                Vector2 positionInScreenSpace = (_pawnPoints[0].value + _pawnPoints[1].value + _pawnPoints[2].value) / 3;
                transform.position = Camera.main.ScreenToWorldPoint(new Vector3(positionInScreenSpace.x, positionInScreenSpace.y, 1));

                EventManager.mOnPawnGoAway.Fire(this);

                mIsPawnLeavingBoard = false;
            }
        }

        #region Find Patern
        /// <summary>
        /// Pawn points keys are corresponding fingerId
        /// If no fingerId is -1, that means no finger represent this pawn point. In that case, the Vector2 position is guessed
        /// </summary>
        private readonly List<Tuple<int, Vector2>> _pawnPoints = new List<Tuple<int, Vector2>>();
        private int _realPawnPointCount = 0;

        /// <summary>
        /// ATTENTION A trier du plus grand au plus petit
        /// </summary>
        public float[] mDistances = new float[3];

        public float mDistancesInterval = 200;

        bool IsPaternMatch(Vector2 a, Vector2 b, Vector2 c)
        {

            List<float> distTemp = new List<float>(mDistances);
            bool foundOne = false;


            

            foreach (var d in distTemp.Where(d => Vector3.Distance(a, b) - d < mDistancesInterval ))
            {
                foundOne = true;
                distTemp.Remove(d);
                break;
            }

            if (!foundOne)
            {
                return false;
            }

            foundOne = false;
            foreach (var d in distTemp.Where(d => Vector3.Distance(a, c) - d < mDistancesInterval))
            {
                foundOne = true;
                distTemp.Remove(d);
                break;
            }

            if (!foundOne)
            {
                return false;
            }

            return distTemp.Any(d => Vector3.Distance(c, b) - d < mDistancesInterval);
        }


        

        /// <summary>
        /// Find pawn pattern on board
        /// </summary>
        void FindPatern()
        {
            FindMissingPoints();

            DeterminePawnPosition();
        }



        /// <summary>
        /// Try to find missing fingers
        /// </summary>
        void FindMissingPoints()
        {
            switch (_realPawnPointCount)
            {
                case 3:
                    // All pawn are founded, do nothing
                    break;
                case 2:
                    Touch[] freeTouches = InputManager.Instance.GetAllUnReservedFingers();
                    List<Tuple<int, Vector2>> pawn = FindDetectedPawn();

                    for (int i = 0; i < freeTouches.Length; i++)
                    {
                        if (IsPaternMatch(freeTouches[i].position, InputManager.Instance.GetTouchFromFingerId(pawn[0].key).position, InputManager.Instance.GetTouchFromFingerId(pawn[1].key).position))
                        {
                            ReserveTouch(freeTouches[i]);

                            return;
                        }
                    }

                    break;
                case 1:
                    freeTouches = InputManager.Instance.GetAllUnReservedFingers();
                    pawn = FindDetectedPawn();

                    for (int i = 0; i < freeTouches.Length - 1; i++)
                    {
                        for (int j = i + 1; j < freeTouches.Length; j++)
                        {
                            if (IsPaternMatch(freeTouches[i].position, freeTouches[j].position, InputManager.Instance.GetTouchFromFingerId(pawn[0].key).position))
                            {
                                ReserveTouch(freeTouches[i]);
                                ReserveTouch(freeTouches[j]);

                                return;
                            }
                        }
                    }
                    break;
                case 0:

                    /* todo mouse input
                    if (Input.GetMouseButtonDown(0))
                    {
                        _pawnPoints[0].value = Input.mousePosition;
                        _pawnPoints[1].value = Input.mousePosition;
                        _pawnPoints[2].value = Input.mousePosition;
                        _isPawnEnteringBoard = true;

                        _realPawnPointCount = 3;

                        return;
                    }*/


                    freeTouches = InputManager.Instance.GetAllUnReservedFingers();

                    for (int i = 0; i < freeTouches.Length - 2; i++)
                    {
                        for (int j = i + 1; j < freeTouches.Length - 1; j++)
                        {
                            for (int k = j + 1; k < freeTouches.Length; k++)
                            {
                                if (IsPaternMatch(freeTouches[i].position, freeTouches[j].position, freeTouches[k].position))
                                {
                                    ReserveTouch(freeTouches[i]);
                                    ReserveTouch(freeTouches[j]);
                                    ReserveTouch(freeTouches[k]);

                                    mIsPawnEnteringBoard = true;

                                    return;
                                }
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        ///  Determine _pawnPoints
        ///  Guess missing points
        /// </summary>
        void DeterminePawnPosition()
        {
            
            for (int i = 0; i < _pawnPoints.Count; i++)
            {
                if (_pawnPoints[i].key != -1)
                    _pawnPoints[i].value = InputManager.Instance.GetTouchFromFingerId(_pawnPoints[i].key).position;
            }

            /*
           switch (_realPawnPointCount)
           {
               case 3:
                   for (int i = 0; i < _pawnPoints.Count; i++)
                   {
                       _pawnPoints[i].value = InputManager.Instance.GetTouchFromFingerId(_pawnPoints[i].key).position;
                   }
                   break;
               case 2:
                   /*
                    * /* TODO something clever to guess points
                    * 
                    *
                   List<Tuple<int, Vector2>> missing = new List<Tuple<int, Vector2>>();
                   List<Tuple<int, Vector2>> fingerDetecters = new List<Tuple<int, Vector2>>();
                   foreach (var p in _pawnPoints)
                   {
                       if (p.key == -1)
                       {
                           missing.Add(p);
                       }
                       else
                       {
                           fingerDetecters.Add(p);
                       }
                   }


                   Vector2 A = fingerDetecters[0].value;
                   Vector2 B = fingerDetecters[1].value;
                   Vector2 C = missing[0].value;

                   float d = Vector2.Distance(C , B);

                   float angle = Vector2.Angle(A - B, C - B);
                    
                  

                   List<Tuple<int, Vector2>> myKeys = FindDetectedPawn();

                   fingerDetecters[0].value = InputManager.Instance.GetTouchFromFingerId(fingerDetecters[0].key).position;
                   fingerDetecters[1].value = InputManager.Instance.GetTouchFromFingerId(fingerDetecters[1].key).position;

                   //_pawnPoints[2] = _pawnPoints[1] + translate;

                   Vector2 dir = (fingerDetecters[0].value - fingerDetecters[1].value).normalized ; // get point direction relative to pivot
                   dir = Rotate(dir, -angle);
                   missing[0].value = dir*d + fingerDetecters[1].value ; // calculate rotated point
                    * *
                   break;
                   
               case 1:
             // Todo correction needed
                   List<Tuple<int, Vector2>> missing = new List<Tuple<int, Vector2>>();
                   List<Tuple<int, Vector2>> fingerDetecters = new List<Tuple<int, Vector2>>();
                   foreach (var p in _pawnPoints)
                   {
                       if (p.key == -1)
                       {
                           missing.Add(p);
                       }
                       else
                       {
                           fingerDetecters.Add(p);
                       }
                   }

                   Vector2 newPos = InputManager.Instance.GetTouchFromFingerId(fingerDetecters[0].key).position;

                   Vector2 translate = newPos - fingerDetecters[0].value;
                   missing[0].value = _pawnPoints[0].value + translate;
                   missing[1].value = _pawnPoints[1].value + translate;
                   fingerDetecters[0].value = newPos;

                   break;
           }
            */
        }

        #endregion

        #region Evaluate Position Direction

        private readonly float[] _angles = new float[3];
        private readonly float[] _distances = new float[3];
        private readonly int[] _fronts = new int[3];
        private int _finalFront = -1;
        private const int FINAL_FRONT_EVALUATE_COUNT = 100;

        void Evaluate()
        {
            if (_pawnPoints[0].value == _pawnPositionOut)
            {
                return;
            }


            int currentFront = -1;
            if (_finalFront == -1)
            {
                EvaluateDistance();
                EvaluateAngle();
                EvaluatePositionOrientation();


                int val = -1;
                for (int i = 0; i < _fronts.Length; i++)
                {
                    if (_fronts[i] > FINAL_FRONT_EVALUATE_COUNT)
                    {
                        _finalFront = i;
                        currentFront = _finalFront;
                        break;
                    }

                    if (_fronts[i] > val)
                    {
                        val = _fronts[i];
                        currentFront = i;
                    }
                }
            }
            else
            {
                currentFront = _finalFront;
            }


            Vector2 positionInScreenSpace = (_pawnPoints[0].value + _pawnPoints[1].value + _pawnPoints[2].value) / 3;

            transform.position = Camera.main.ScreenToWorldPoint(new Vector3(positionInScreenSpace.x, positionInScreenSpace.y, 1));

            Vector3 direction = _pawnPoints[currentFront].value - positionInScreenSpace;

            transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90));
        }

        void EvaluatePositionOrientation()
        {
            {
                // Find the two closest distances to find the current front
                float distance01 = Math.Abs(_distances[0] - _distances[1]);
                float distance02 = Math.Abs(_distances[0] - _distances[2]);
                float distance21 = Math.Abs(_distances[2] - _distances[1]);

                if (distance01 < distance02 && distance01 < distance21)
                {
                    _fronts[0]++;
                    //_currentFront = 0;
                }
                else if (distance02 < distance01 && distance02 < distance21)
                {
                    _fronts[1]++;
                    //_currentFront = 1;
                }
                else if (distance21 < distance01 && distance21 < distance02)
                {
                    _fronts[2]++;
                    //_currentFront = 2;
                }
            }


            {
                // evaluate the shortest Angle to find the current front
                float a = Math.Abs(_angles[0] - _angles[1]);
                float b = Math.Abs(_angles[0] - _angles[2]);
                float c = Math.Abs(_angles[2] - _angles[1]);

                if (a < b && a < c)
                {

                    _fronts[2]++;
                    //_currentFront = 2;
                }
                else if (b < a && b < c)
                {
                    _fronts[1]++;
                    //_currentFront = 1;

                }
                else if (c < a && c < b)
                {
                    _fronts[0]++;
                    //_currentFront = 0;
                }
            }



        }

        void EvaluateDistance()
        {

#if IN_REAL_WORLD_SPACE
            // Convert distance in Real World PositionInScreenSpace
             Vector2[] worldTouchesPosition = new Vector2[3];

            worldTouchesPosition[0] = new Vector2(_pawnPoints[0].position.x * _facteurWidth, _pawnPoints[0].position.y * _facteurHeight);
            worldTouchesPosition[1] = new Vector2(_pawnPoints[1].position.x * _facteurWidth, _pawnPoints[1].position.y * _facteurHeight);
            worldTouchesPosition[2] = new Vector2(_pawnPoints[2].position.x * _facteurWidth, _pawnPoints[2].position.y * _facteurHeight);

            _distances[0] = Vector2.Distance(worldTouchesPosition[1], worldTouchesPosition[0]);
            _distances[1] = Vector2.Distance(worldTouchesPosition[2], worldTouchesPosition[0]);
            _distances[2] = Vector2.Distance(worldTouchesPosition[1], worldTouchesPosition[2]);
#else
            _distances[0] = Vector2.Distance(_pawnPoints[1].value, _pawnPoints[0].value);
            _distances[1] = Vector2.Distance(_pawnPoints[2].value, _pawnPoints[0].value);
            _distances[2] = Vector2.Distance(_pawnPoints[1].value, _pawnPoints[2].value);
#endif

        }

        void EvaluateAngle()
        {
            Vector2 direction01 = (_pawnPoints[1].value - _pawnPoints[0].value);
            Vector2 direction10 = (_pawnPoints[0].value - _pawnPoints[1].value);
            Vector2 direction12 = (_pawnPoints[2].value - _pawnPoints[1].value);
            Vector2 direction21 = (_pawnPoints[1].value - _pawnPoints[2].value);
            Vector2 direction02 = (_pawnPoints[2].value - _pawnPoints[0].value);
            Vector2 direction20 = (_pawnPoints[0].value - _pawnPoints[2].value);

            _angles[0] = Vector2.Angle(direction01, direction02);
            _angles[1] = Vector2.Angle(direction12, direction10);
            _angles[2] = Vector2.Angle(direction20, direction21);
        }

        #endregion

#if DEBUG
        void OnGUI()
        {

            Vector2 offset = new Vector2(0, 0);

            // Display Angles
            GUI.Label(
                new Rect(_pawnPoints[0].value.x + offset.x,
                    UnityEngine.Screen.height - _pawnPoints[0].value.y + offset.y, 100, 100), "0: " + _angles[0]);
            GUI.Label(
                new Rect(_pawnPoints[1].value.x + offset.x,
                    UnityEngine.Screen.height - _pawnPoints[1].value.y + offset.y, 100, 100), "1: " + _angles[1]);
            GUI.Label(
                new Rect(_pawnPoints[2].value.x + offset.x,
                    UnityEngine.Screen.height - _pawnPoints[2].value.y + offset.y, 100, 100), "2: " + _angles[2]);

            // Display Distances
            GUI.Label(new Rect((_pawnPoints[0].value.x + _pawnPoints[1].value.x) / 2 + offset.x,
                UnityEngine.Screen.height - (_pawnPoints[0].value.y + _pawnPoints[1].value.y) / 2 + offset.y, 100, 100),
                "0: " + _distances[0]);
            GUI.Label(new Rect((_pawnPoints[0].value.x + _pawnPoints[2].value.x) / 2 + offset.x,
                UnityEngine.Screen.height - (_pawnPoints[0].value.y + _pawnPoints[2].value.y) / 2 + offset.y, 100, 100),
                "1: " + _distances[1]);
            GUI.Label(new Rect((_pawnPoints[2].value.x + _pawnPoints[1].value.x) / 2 + offset.x,
                UnityEngine.Screen.height - (_pawnPoints[2].value.y + _pawnPoints[1].value.y) / 2 + offset.y, 100, 100),
                "2: " + _distances[2]);


            // Display Front
            //   GUI.Label(new Rect(_pawnPoints[_currentFront].x, UnityEngine.Screen.height - _pawnPoints[_currentFront].y, 100, 100), "front");
        }
#endif


        /* 
        * Now Useless since Real Distance convertion is working
        * void TEST_EvaluateRealDistance()
        {
            Vector2[] realWorldTouchesPosition = new Vector2[2];

            realWorldTouchesPosition[0] = new Vector2(_pawnPoints[0].position.x * _facteurWidth,
                _pawnPoints[0].position.y * _facteurHeight);
            realWorldTouchesPosition[1] = new Vector2(_pawnPoints[1].position.x * _facteurWidth,
                _pawnPoints[1].position.y * _facteurHeight);

            _realDistanceTest = Vector2.Distance(realWorldTouchesPosition[0], realWorldTouchesPosition[1]);
          
             // Display Real Distance Test
             //GUI.Label(new Rect(30, 40, 100, 100), "Real distance test: " + _realDistanceTest);
        }*/
    }
}
