﻿#define DEBUG

using System;
using System.Collections.Generic;
using Assets.commons.Scripts;
using Assets.commons.Scripts.Entity;
using Assets.Scripts.Managers;
using Assets.Scripts.Screen;
using Assets.Scripts.Utils.TilesModifiers;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class Targeting : Singleton<Targeting>
    {
        private LineRenderer _lines;

        private int _bounceCount;
      //  private bool _bounceOnDestructibleWalls;
        private TargetingMode _targetingMode;

 

        private int _lineRendererVertexCount;

        private readonly List<Result> _currentResults = new List<Result>(); 

        private GameObject _objectTargeting;
        private Vector2 _objectTargetingOrigine;

        public enum TargetingMode
        {
            Ray,
            Cone
        }

        /// <summary>
        /// contain informations about the object collided in the way
        /// -1: no information
        /// </summary>
        public struct Result
        {
            public GameObject mHitted;
            public int mDistance;
            public int mBounce;
        }

        public void Start()
        {
            _lines = GetComponent<LineRenderer>();
           
            enabled = false;
            _lines.enabled = false;
        }

        /// <summary>
        /// Initialize targeting mode.
        /// </summary>
        /// <param name="objectTargeting">The origine. It fixe the origine position to the middle case</param>
        /// <param name="targetingMode"></param>
        /// <param name="bounceCount">Number of maximum bounce to touch</param>
        public void ActiveTargeting(GameObject objectTargeting, TargetingMode targetingMode = TargetingMode.Ray, int bounceCount = 0)
        {
            enabled = true;
            _lines.enabled = true;
            AccessibilityMap.Instance.ComputePath(GridManager.Instance.WorldToTile(objectTargeting.transform.position), 999);

            this._bounceCount = bounceCount;
          
            this._targetingMode = targetingMode;

            this._objectTargeting = objectTargeting;
            this._objectTargetingOrigine =
                GridManager.Instance.TileToWorldMiddle(GridManager.Instance.WorldToTile(_objectTargeting.transform.position));
            Debug.Log(GridManager.Instance.WorldToTile(_objectTargeting.transform.position));
            _lineRendererVertexCount = _bounceCount + 2;
           _lines.SetVertexCount(_lineRendererVertexCount);
           
        }

        /// <summary>
        /// stop targeting mode and return a list of hitted object
        /// </summary>
        /// <returns>For a ray: hitted list objects. Ordered from the origine to the last object hitted</returns>
        public List<Result> HandleResult()
        {
            enabled = false;
            _lines.enabled = false;

            GridDrawer.Instance.Uncolorize(Color.red);

            return _currentResults;
        }
	
        // Update is called once per frame
        void Update()
        {
            _lineCurrent = 0;
            _currentResults.Clear();;

            GridDrawer.Instance.Uncolorize(Color.red);

            switch (_targetingMode)
            {
                case TargetingMode.Ray:
                    RayBounce(_objectTargetingOrigine, _objectTargeting.transform.right);
                    break;
                case TargetingMode.Cone:
                    Cone(_objectTargetingOrigine, _objectTargeting.transform.right);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            
        }

        private int _lineCurrent;

        // todo
        private void Cone(Vector2 position, Vector2 direction)
        {
            
        }

        // Assert : destructible can't be a hole
        private void RayBounce(Vector2 position, Vector2 direction)
        {
            int bounceLeft = _bounceCount;
            while (bounceLeft >= 0)
            {
                _lines.SetPosition(_lineCurrent, position);
                _lineCurrent++;

                RaycastHit2D[] hits = Physics2D.RaycastAll(position, direction);

                bool somethingHitted = false;
                foreach (var h in hits)
                {
                    AddObjectToHistory(bounceLeft, h);

                    Unit d = h.collider.GetComponent<Unit>();
                    TileModifierWalkability tm = h.collider.GetComponent<TileModifierWalkability>();

                    if (d)
                    {
                        /*if (_bounceOnDestructibleWalls && tm)
                        {
                            // Bounce
                            direction = Vector3.Reflect(direction, h.normal);
                            position = h.point + direction * 0.1f;
                            bounceLeft--;
                            somethingHitted = true;
                            break;
                        }
                        else
                        {*/
                            // Touch

                            
                            position = h.point;
                            GridDrawer.Instance.Colorize(GridManager.Instance.WorldToTile(h.point), Color.red);
                            bounceLeft = -1;
                            somethingHitted = true;
                            
                            break;
                        //}
                    }
                    else if (tm)
                    {
                        if (tm.WalkabilityState != TileModifierWalkability.Walkability.Hole)
                        {
                            // Bounce
                            direction = Vector3.Reflect(direction, h.normal);
                            position = h.point + direction*0.1f;
                            bounceLeft--;
                            somethingHitted = true;
                            break;
                        }
                        else
                        {
                            // Continu
                            
                        }
                    }
                    else
                    {
                        // object not deffined
                        
                        // Continu
                    }
                }


                if (!somethingHitted)
                {
                    position = position + direction * 100;
                    for (int i = _lineCurrent; i < _lineRendererVertexCount - 1; i++)
                    {
                        _lines.SetPosition(_lineCurrent, position);
                        _lineCurrent++;
                    }

                    break;
                }


               
            }

            for (int i = _lineCurrent; i < _lineRendererVertexCount; i++)
            {

                _lines.SetPosition(_lineCurrent, position);
                _lineCurrent++;
            }
        }


        void AddObjectToHistory(int bounceLeft, RaycastHit2D h)
        {
            Result r = new Result
            {
                mBounce = _bounceCount - bounceLeft,
                
                
                mHitted = h.collider.gameObject
            };

            if (r.mBounce > 0)
            {
                r.mDistance = -1;
            }
            else
            {
                r.mDistance = AccessibilityMap.Instance.GetPath(GridManager.Instance.WorldToTile(h.point)).Count;
            }
                

            _currentResults.Add(r);
        }

    }



}




