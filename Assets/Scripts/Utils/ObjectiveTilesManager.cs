﻿using System.Collections.Generic;
using System.Linq;
using Assets.commons.Scripts;
using Assets.Scripts.Managers;

namespace Assets.Scripts.Utils
{
    public class ObjectiveTilesManager : Singleton<ObjectiveTilesManager>
    {
        private Dictionary<int, HashSet<GridManager.Tile>> _zones;
        private bool _otherZonesLocked = false;
        public bool ExitUnlocked { get; private set; }

        public override void Awake()
        {
            base.Awake();
            _zones = new Dictionary<int, HashSet<GridManager.Tile>>();
            foreach (var tile in GridManager.Instance.TilesList.Where(tile => tile.IsExit))
            {
                if (!_zones.ContainsKey(tile.ExitID))
                {
                    _zones.Add(tile.ExitID, new HashSet<GridManager.Tile>());
                }
                _zones[tile.ExitID].Add(tile);
            }
            ExitUnlocked = false;
        }

        public List<GridManager.Tile> ZoneTiles(int exitID)
        {
            return !_zones.ContainsKey(exitID) ? new List<GridManager.Tile>() : _zones[exitID].ToList();
        }

        public void ObjectiveZoneReached(GridManager.Tile tile)
        {
            if (_otherZonesLocked)
            {
                return;
            }
            _otherZonesLocked = true;
            foreach (var t in _zones.Keys.Where(key => key != tile.ExitID || key == 0).SelectMany(key => _zones[key]))
            {
                t.IsExit = false;
            }
            tile.IsExit = true; // We force this to true in case it has been deactivated (because of the key == 0 condition).
            ExitUnlocked = true;
        }
    }
}
