﻿// ----------------------------------------------------------------------------
// Tuple structs for use in .NET Not-Quite-3.5 (e.g. Unity3D).
//
// Used Chapter 3 in http://functional-programming.net/ as a starting point.
//
// Note: .NET 4.0 Tuples are immutable classes so they're *slightly* different.
// ----------------------------------------------------------------------------

namespace Assets.Scripts.Utils
{
   
    public class Tuple<T1,T2>
    {
        public T1 key;
        public T2 value;

        public Tuple(T1 key, T2 value)
        {
            this.key = key;
            this.value = value;
        }
    }
}