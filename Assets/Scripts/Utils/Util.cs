﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class Util {
        public static Quaternion OrientationToQuaternion(Orientation orientation)
        {
            switch (orientation)
            {
                case Orientation.Left:
                    return Quaternion.Euler(0, 180, 0);
                case Orientation.Right:
                    return Quaternion.Euler(0, 0, 0);
                case Orientation.Up:
                    return Quaternion.Euler(0, -90, 0);
                case Orientation.Down:
                    return Quaternion.Euler(0, 90, 0);
            }
            return Quaternion.identity;
        }
    }
}
