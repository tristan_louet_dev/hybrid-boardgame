﻿using Assets.commons.Scripts.Entity;
using Assets.Scripts.Managers;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.Utils.TilesModifiers.Spawners
{
    /// <summary>
    /// Turn-based spawner.
    /// </summary>
    public class TurnSpawner : Spawner
    {
        public enum ResetOn
        {
            AfterPlayersTurn,
            AfterPlayerSpawn,
            AfterPlayerTurn,
            AfterAITurn,
            AfterEnemyTurn
        }

        public enum SpawnOn
        {
            Activation,
            BeforePlayersTurn,
            BeforePlayerSpawn,
            AfterPlayerSpawn,
            BeforePlayerTurn,
            AfterPlayerTurn,
            AfterPlayersTurn,
            BeforeAITurn,
            BeforeEnemyTurn,
            AfterEnemyTurn,
            AfterAITurn
        }

        /// <summary>
        /// Number of times it can be activated.
        /// </summary>
        [SerializeField]
        private int _repeatability;

        /// <summary>
        /// Get the number of times it can be activated.
        /// </summary>
        public int Repeatability
        {
            get { return _repeatability; }
        }

        /// <summary>
        /// Get the current number of times it can be activated.
        /// </summary>
        public int CurrentRepeatability { get; private set; }

        /// <summary>
        /// Number of turns before it can be activated again.
        /// 0 = Activate for each individual player turn.
        /// </summary>
        [SerializeField]
        private int _turnBeforeReset;

        /// <summary>
        /// Current number of turns before next activation.
        /// </summary>
        private int _currentTurnBeforeReset;

        /// <summary>
        /// Indicate when the spawner reset counter is decremented.
        /// </summary>
        [SerializeField]
        private List<ResetOn> _resetOn;

        /// <summary>
        /// Indicate when the enemy can spawn.
        /// </summary>
        [SerializeField]
        private List<SpawnOn> _spawnOn;

        /// <summary>
        /// If the enemy can spawn.
        /// </summary>
        private bool _canSpawn;

        /// <summary>
        /// Register events.
        /// </summary>
        private void Awake()
        {
            CurrentRepeatability = _repeatability;
            EventManager.mOnPlayersTurnStarted += OnPlayersTurnStarted;
            EventManager.mOnPlayerSpawning += OnPlayerSpawning;
            EventManager.mOnPlayerSpawned += OnPlayerSpawned;
            EventManager.mOnPlayerTurnStarted += OnPlayerTurnStarted;
            EventManager.mOnPlayerTurnFinished += OnPlayerTurnFinished;
            EventManager.mOnPlayersTurnFinished += OnPlayersTurnFinished;
            EventManager.mOnAITurnStarted += OnAITurnStarted;
            EventManager.mOnEnemyTurnStarted += OnEnemyTurnStarted;
            EventManager.mOnEnemyTurnFinished += OnEnemyTurnFinished;
            EventManager.mOnAITurnFinished += OnAITurnFinished;
        }

        /// <summary>
        /// Unregister events.
        /// </summary>
        private void OnDestroy()
        {
            EventManager.mOnPlayersTurnStarted -= OnPlayersTurnStarted;
            EventManager.mOnPlayerSpawning -= OnPlayerSpawning;
            EventManager.mOnPlayerSpawned -= OnPlayerSpawned;
            EventManager.mOnPlayerTurnStarted -= OnPlayerTurnStarted;
            EventManager.mOnPlayerTurnFinished -= OnPlayerTurnFinished;
            EventManager.mOnPlayersTurnFinished -= OnPlayersTurnFinished;
            EventManager.mOnAITurnStarted -= OnAITurnStarted;
            EventManager.mOnEnemyTurnStarted -= OnEnemyTurnStarted;
            EventManager.mOnEnemyTurnFinished -= OnEnemyTurnFinished;
            EventManager.mOnAITurnFinished -= OnAITurnFinished;
        }

        /// <summary>
        /// Called when before players turns.
        /// </summary>
        private void OnPlayersTurnStarted()
        {
            CheckCanSpawn(SpawnOn.BeforePlayersTurn);
            Spawn();
        }

        /// <summary>
        /// Called when a player spawns.
        /// </summary>
        private void OnPlayerSpawning(Character character)
        {
            CheckCanSpawn(SpawnOn.BeforePlayerSpawn);
            Spawn();
        }

        /// <summary>
        /// Called when a player spawned.
        /// </summary>
        private void OnPlayerSpawned(Character character)
        {
            CheckCanSpawn(SpawnOn.AfterPlayerSpawn);
            CheckReset(ResetOn.AfterPlayerSpawn);
            Spawn();
        }

        /// <summary>
        /// Called when a player starts its turn.
        /// </summary>
        private void OnPlayerTurnStarted(Character character)
        {
            CheckCanSpawn(SpawnOn.BeforePlayerTurn);
            Spawn();
        }

        /// <summary>
        /// Called when a player finished its turn.
        /// </summary>
        private void OnPlayerTurnFinished(Character character)
        {
            CheckCanSpawn(SpawnOn.AfterPlayerTurn);
            CheckReset(ResetOn.AfterPlayerTurn);
            Spawn();
        }

        /// <summary>
        /// Called when after players turns.
        /// </summary>
        private void OnPlayersTurnFinished()
        {
            CheckCanSpawn(SpawnOn.AfterPlayersTurn);
            CheckReset(ResetOn.AfterPlayersTurn);
            Spawn();
        }

        /// <summary>
        /// Called when AI starts its turn.
        /// </summary>
        private void OnAITurnStarted()
        {
            CheckCanSpawn(SpawnOn.BeforeAITurn);
            Spawn();
        }

        /// <summary>
        /// Called when an enemy starts its turn.
        /// </summary>
        private void OnEnemyTurnStarted()
        {
            CheckCanSpawn(SpawnOn.BeforeEnemyTurn);
            Spawn();
        }

        /// <summary>
        /// Called when an enemy finished its turn.
        /// </summary>
        private void OnEnemyTurnFinished()
        {
            CheckCanSpawn(SpawnOn.AfterEnemyTurn);
            CheckReset(ResetOn.AfterEnemyTurn);
            Spawn();
        }

        /// <summary>
        /// Called when AI finished its turn.
        /// </summary>
        private void OnAITurnFinished()
        {
            CheckCanSpawn(SpawnOn.AfterAITurn);
            CheckReset(ResetOn.AfterAITurn);
            Spawn();
        }

        /// <summary>
        /// Check if the enemy can spawn.
        /// </summary>
        /// <param name="turnType">Current turn type.</param>
        private void CheckCanSpawn(SpawnOn turnType)
        {
            _canSpawn = _spawnOn.Contains(SpawnOn.Activation) || _spawnOn.Contains(turnType);
        }

        /// <summary>
        /// Check if the spawner is reseted.
        /// </summary>
        /// <param name="turnType">Current turn type.</param>
        private void CheckReset(ResetOn turnType)
        {
            // If not already reseted.
            if (_currentTurnBeforeReset <= 0) return;
            if (_resetOn.Contains(turnType))
            {
                _currentTurnBeforeReset--;
            }
        }

        /// <summary>
        /// Spawn an enemy.
        /// </summary>
        /// <returns>Spawned enemy.</returns>
        protected override GameObject Spawn()
        {
            // Destroy when it can't be activated anymore.
            if (CurrentRepeatability == 0 && Repeatability > 0)
            {
                Destroy(gameObject);
                return null;
            } else if (!_canSpawn || _currentTurnBeforeReset > 0)
            {
                return null;
            }
            var spawned = base.Spawn();
            // Reset.
            if (Repeatability > 0)
            {
                --CurrentRepeatability;
                if (CurrentRepeatability == 0)
                {
                    Destroy(gameObject);
                    return spawned;
                }
            }
            _currentTurnBeforeReset = _turnBeforeReset;
            return spawned;
        }

        /// <summary>
        /// Indicate if the enemy can spawn.
        /// </summary>
        /// <param name="type"></param>
        /// <returns>If he can spawn.</returns>
        protected bool CanSpawnOn(SpawnOn type)
        {
            return _spawnOn.Contains(type);
        }
    }
}
