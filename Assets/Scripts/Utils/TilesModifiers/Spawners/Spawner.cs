﻿using System.Collections;
using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;
using Assets.commons.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Utils.TilesModifiers.Spawners
{
    /// <summary>
    /// Handler for spawn.
    /// </summary>
    public abstract class SpawnHandler : Handler<IEnumerator, GameObject>
    {
    }

    [ExecuteInEditMode]
    public abstract class Spawner : TileModifier
    {
        /// <summary>
        /// Enemy to spawn.
        /// </summary>
        [SerializeField]
        private GameObject _enemy;

        /// <summary>
        /// Tile to spawn the enemy on.
        /// </summary>
        [SerializeField]
        private Vector2 _tile;

        /// <summary>
        /// Orientation of spawned enemy.
        /// </summary>
        [SerializeField]
        private Orientation _orientation;

        /// <summary>
        /// PV of spawned enemy.
        /// </summary>
        [SerializeField]
        private int _pv;

        /// <summary>
        /// Damages of spawned enemy.
        /// </summary>
        [SerializeField]
        private int _damages;

        /// <summary>
        /// An handler for when the enemy is spawned.
        /// </summary>
        [SerializeField]
        private SpawnHandler _handler;

        /// <summary>
        /// Spawn an enemy.
        /// </summary>
        /// <returns>Spawned enemy.</returns>
        protected virtual GameObject Spawn()
        {
            if (_enemy == null) return null;
            var spawned = (GameObject)Instantiate(_enemy);
            spawned.GetComponent<Unit>().Tile = _tile;
            spawned.transform.rotation = Util.OrientationToQuaternion(_orientation);
            Debug.Log("Spawned enemy " + _enemy + " at " + _tile);
            if (_handler == null) return spawned;
            // Deactivate the spawned enemy.
            spawned.SetActive(false);
            // Automatically pause the game to execute the handler.
            GameManager.Instance.PauseToExecute(_handler.Handle(spawned));
            return spawned;
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying)
            {
                _tile = GridManager.Instance.WorldToTile(transform.position);
            }
        }
#endif

        public override Color GridEditorColor
        {
            get { return new Color(0.49f, 0, 1, 0.5f); }
        }

        public override void Action(GridManager.Tile tile)
        {
        }
    }
}
