﻿using UnityEngine;

namespace Assets.Scripts.Utils.TilesModifiers.Spawners
{
    /// <summary>
    /// Spawner for when the level start.
    /// </summary>
    public class StartSpawner : Spawner
    {

        private int i = 0;

        /// <summary>
        /// Spawn the enemy when the level start.
        /// </summary>
        private void Start()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif
            Spawn();
            Destroy(gameObject);
        }

    }
}
