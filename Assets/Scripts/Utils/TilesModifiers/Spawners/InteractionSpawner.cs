﻿using Assets.commons.Scripts.Utils;
using Assets.Scripts.Interactions;
using UnityEngine;

namespace Assets.Scripts.Utils.TilesModifiers.Spawners
{
    /// <summary>
    /// Spawner for when an interactable object has been activated.
    /// </summary>
    [RequireComponent(typeof(SpawnInteraction))]
    public class InteractionSpawner : TurnSpawner, IHandler<InteractionHandler.Result, Vector2>
    {
        /// <summary>
        /// If the spawner is active.
        /// </summary>
        [SerializeField]
        private bool _active;

        /// <summary>
        /// Activate/Deactivate the spawner on interaction.
        /// </summary>
        /// <param name="tile">Tile coordinates.</param>
        /// <returns>Result.</returns>
        public InteractionHandler.Result Handle(Vector2 tile)
        {
            _active = !_active;
            if (CanSpawnOn(SpawnOn.Activation))
            {
                Spawn();
            }
            return (CurrentRepeatability == 0 && Repeatability > 0) ? InteractionHandler.Result.Remove : InteractionHandler.Result.Done;
        }

        /// <summary>
        /// Spawn only if the spawner is active.
        /// </summary>
        /// <returns>Spawned enemy.</returns>
        protected override GameObject Spawn()
        {
            return !_active ? null : base.Spawn();
        }
    }
}
