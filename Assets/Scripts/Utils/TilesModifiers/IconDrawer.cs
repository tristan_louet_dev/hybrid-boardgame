﻿using UnityEngine;

namespace Assets.Scripts.Utils.TilesModifiers
{
    [ExecuteInEditMode]
    public class IconDrawer : MonoBehaviour
    {
        /// <summary>
        /// Icon to draw.
        /// </summary>
        public string mIcon;

        /// <summary>
        /// Draw the icon.
        /// </summary>
        private void OnDrawGizmos()
        {
            if (mIcon == null) return;
            Gizmos.DrawIcon(transform.position, mIcon);
        }
    }
}
