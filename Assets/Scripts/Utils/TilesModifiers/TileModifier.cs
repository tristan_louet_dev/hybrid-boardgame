﻿using Assets.Scripts.Managers;
using Assets.Scripts.Screen;
using UnityEngine;

namespace Assets.Scripts.Utils.TilesModifiers
{
    [ExecuteInEditMode]
    public abstract class TileModifier : MonoBehaviour
    {
        public abstract Color GridEditorColor { get; }

        public Color GridEditorColorError
        {
            get { return new Color(0.52f, 0.02f, 0.02f, 0.5f); }
        }

        public abstract void Action(GridManager.Tile tile);

        public virtual void Action(Rect bounds)
        {
            Vector2 tilePosition = new Vector2();
            for (var j = 0; j < bounds.height; ++j)
            {
                for (var i = 0; i < bounds.width; ++i)
                {
                    tilePosition.x = (int)(bounds.x + i);
                    tilePosition.y = (int)(bounds.y + j);

                    if (!GridManager.Instance.Contains(tilePosition))
                    {
                        continue;
                    }
                    var tile = GridManager.Instance.TileAt(tilePosition);

#if UNITY_EDITOR
                    if (!Application.isPlaying)
                    {
                        GridDrawer.Instance.Colorize(tile, GridEditorColor);
                    }
#endif

                    Action(tile);
                }
            }
        }

        void Start()
        {
#if UNITY_EDITOR
            if (Application.isEditor)
            {
                transform.parent = GridManager.Instance.mSceneRoot.transform;
            }
#endif
        }
    }
}
