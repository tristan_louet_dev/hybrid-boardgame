﻿using Assets.Scripts.Managers;
using Assets.Scripts.Screen;
using UnityEngine;

namespace Assets.Scripts.Utils.TilesModifiers
{
    /// <summary>
    /// Modify how a tile interact with players and others elements.
    /// </summary>
    public class TileModifierSpeed : TileModifier
    {
        /// <summary>
        /// Change the tile cost to enter
        /// </summary>
        public int mCostToComeIn;

        public override Color GridEditorColor
        {
            get { return new Color(0.2f, 0.2f, 0.2f, 0.3f); }
        }

        public override void Action(GridManager.Tile tile)
        {
            if (tile.mCostToComeIn < mCostToComeIn)
                tile.mCostToComeIn = mCostToComeIn;

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                GridDrawer.Instance.Colorize(tile, GridEditorColor);
            }
#endif
        }
    }
}
