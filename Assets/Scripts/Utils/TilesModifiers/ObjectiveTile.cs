﻿using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Utils.TilesModifiers
{
    public class ObjectiveTile : TileModifier
    {
        /// <summary>
        /// Indicates to which zone this objective tile belongs.
        /// </summary>
        [SerializeField]
        private int _groupIndex;

        public override Color GridEditorColor
        {
            get { return new Color(1, 0, 0, 0.5f); }
        }

        public override void Action(GridManager.Tile tile)
        {
            tile.IsExit = true;
            tile.ExitID = _groupIndex;
        }
    }
}
