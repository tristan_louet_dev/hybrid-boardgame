﻿using System;
using Assets.Scripts.Managers;
using Assets.Scripts.Screen;
using UnityEngine;

namespace Assets.Scripts.Utils.TilesModifiers
{
    /// <summary>
    /// Modify how a tile interact with players and others elements.
    /// </summary>
    public class TileModifierWalkability : TileModifier
    {
        
        [SerializeField]
        [HideInInspector]
        private Walkability _walkability;

        /// <summary>
        /// Define wether the tile is solid or not.
        /// </summary>
        [ExposeProperty]
        public Walkability WalkabilityState
        {
            get
            {
                return _walkability;
            }
            set
            {
          /*      var boxCollider2D = GetComponent<BoxCollider2D>();
                if (value == Walkability.Hole)
                {
                    if (boxCollider2D != null)
                    {
                        DestroyImmediate(boxCollider2D);
                    }
                }
                else
                {
                    if (boxCollider2D == null)
                        gameObject.AddComponent<BoxCollider2D>();
                }*/

                _walkability = value;
            }
        }

        [Flags]
        public enum Walkability
        {
            Block,
            Wall,
            Hole
        }

        public override Color GridEditorColor
        {
            get { return new Color(0, 0, 1, 0.5f); }
        }

        public Color GridEditorColorHole
        {
            get { return new Color(0, 0, 0, 0.5f); }
        }


        public override void Action(Rect bounds)
        {
            switch (_walkability)
            {
                case Walkability.Wall:
                    WallAlgorithm(bounds);
                    break;
                default:
                    base.Action(bounds);
                    break;
            }


        }


        public override void Action(GridManager.Tile tile)
        {
            switch (_walkability)
            {
                case Walkability.Block:
                    tile.mComeFromDown = false;
                    tile.mComeFromLeft = false;
                    tile.mComeFromRight = false;
                    tile.mComeFromUp = false;

#if UNITY_EDITOR
                    if (!Application.isPlaying)
                    {
                        GridDrawer.Instance.Colorize(tile, GridEditorColor);
                    }
#endif

                    break;
                case Walkability.Wall:
                    // already done
                    break;
                case Walkability.Hole:
                    tile.mIsHole = true;

#if UNITY_EDITOR
                    if (!Application.isPlaying)
                    {
                        GridDrawer.Instance.Colorize(tile, GridEditorColorHole);
                    }
#endif
                    

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }



        }


        /// <summary>
        /// Pass throw occupied tile to compute the wall
        /// </summary>
        /// <param name="bounds"></param>
        void WallAlgorithm(Rect bounds)
        {
            Vector2 tilePosition = new Vector2();
            // Allongement horizontal
            if ((int)bounds.height == 2 && (int)bounds.width != 2)
            {
                for (var x = 0; x < bounds.width; ++x)
                {

                    tilePosition.x = (int)(bounds.x + x);
                    tilePosition.y = (int)(bounds.y + 0);

                    if (!GridManager.Instance.Contains(tilePosition))
                    {
                        continue;
                    }

                    var tile = GridManager.Instance.TileAt(tilePosition);
                    tile.mComeFromUp = false;
                    Action(tile);


                    tilePosition.x = (int)(bounds.x + x);
                    tilePosition.y = (int)(bounds.y + 1);

                    if (!GridManager.Instance.Contains(tilePosition))
                    {
                        continue;
                    }

                    tile = GridManager.Instance.TileAt(tilePosition);
                    tile.mComeFromDown = false;
                    Action(tile);

#if UNITY_EDITOR
                    if (!Application.isPlaying)
                    {
                        GridDrawer.Instance.ColorizeLineHorizontal(tilePosition, GridEditorColor);
                    }
#endif

                }
            }
            // Allongement vertical
            else if ((int)bounds.width == 2 && (int)bounds.height != 2)
            {

                for (var y = 0; y < bounds.height; ++y)
                {

                    tilePosition.x = (int)(bounds.x + 0);
                    tilePosition.y = (int)(bounds.y + y);

                    if (!GridManager.Instance.Contains(tilePosition))
                    {
                        continue;
                    }

                    var tile = GridManager.Instance.TileAt(tilePosition);
                    tile.mComeFromRight = false;
                    Action(tile);

                    tilePosition.x = (int)(bounds.x + 1);
                    tilePosition.y = (int)(bounds.y + y);

                    if (!GridManager.Instance.Contains(tilePosition))
                    {
                        continue;
                    }

                    tile = GridManager.Instance.TileAt(tilePosition);
                    tile.mComeFromLeft = false;
                    Action(tile);


#if UNITY_EDITOR
                    if (!Application.isPlaying)
                    {
                        GridDrawer.Instance.ColorizeLineVertical(tilePosition, GridEditorColor);
                    }
#endif
                }
            }
            else
            {
                //Debug.LogError("Walls can't be placed here");
                // draw red color for display
                for (var x = 0; x < bounds.width; ++x)
                {
                    for (var y = 0; y < bounds.height; ++y)
                    {
#if UNITY_EDITOR
                        if (!Application.isPlaying)
                        {
                            tilePosition.x = (int)(bounds.x + x);
                            tilePosition.y = (int)(bounds.y + y);
                            GridDrawer.Instance.Colorize(tilePosition,
                                GridEditorColorError);
                        }
#endif
                    }
                }
            }
        }

    }
}
