﻿using System;
using System.Collections.Generic;
using Assets.commons.Scripts.Utils;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Utils.TilesModifiers
{
    /// <summary>
    /// Enum for activation modes.
    /// </summary>
    [Flags]
    public enum ActivationMode
    {
        Interaction = 0x1,
        Damage = 0x2,
        Electroshock = 0x4
    }

    /// <summary>
    /// Enum for activation sides.
    /// </summary>
    [Flags]
    public enum ActivationSide
    {
        Left = 0x1,
        Right = 0x2,
        Top = 0x4,
        Bottom = 0x8,
        All = Left | Right | Top | Bottom
    }

    /// <summary>
    /// Handler for interactions.
    /// </summary>
    public abstract class InteractionHandler : Handler<InteractionHandler.Result, Vector2>
    {
        /// <summary>
        /// Interaction result.
        /// </summary>
        [Flags]
        public enum Result
        {
            Done = 0x0,     // Interaction has been handled.
            Stop = 0x1,     // Stop the interaction propagation.
            Remove = 0x2    // Remove the interactable game object.
        }
    }

    /// <summary>
    /// Mark a game object as interactable.
    /// </summary>
    public class Interactable : TileModifier
    {
        /// <summary>
        /// Indicate that the interactable game object will not reset.
        /// </summary>
        private const int NO_RESET = 999;

        /// <summary>
        /// Interaction handler.
        /// </summary>
        [SerializeField]
        private List<InteractionHandler> _handlers = null;

            /// <summary>
        /// Activation mode.
        /// </summary>
        [SerializeField]
        private List<ActivationMode> _activationMode;

        /// <summary>
        /// List of activation sides.
        /// </summary>
        [SerializeField]
        private List<ActivationSide> _activationSides;

        /// <summary>
        /// Cost for activating this interactable game object.
        /// </summary>
        [SerializeField]
        private int _activationCost;

        /// <summary>
        /// Number of times it can be interacted with.
        /// </summary>
        [SerializeField]
        private int _repeatability;

        /// <summary>
        /// Number of turns before the game object can be interacted with again.
        /// </summary>
        [SerializeField]
        private int _turnBeforeReset;

        /// <summary>
        /// Current number of turns before the reset.
        /// </summary>
        private int _currentTurnBeforeReset;

        /// <summary>
        /// If the interactable game object can be activated.
        /// </summary>
        private bool _activated = true;

        private void Awake()
        {
            EventManager.mOnGameTurnStarted += OnGameTurnStarted;
        }

        private void OnDestroy()
        {
            EventManager.mOnGameTurnStarted -= OnGameTurnStarted;
        }

        /// <summary>
        /// When a game turn has been started.
        /// </summary>
        private void OnGameTurnStarted()
        {
            if (_activated || _turnBeforeReset == NO_RESET) return;
            --_currentTurnBeforeReset;
            if (_currentTurnBeforeReset != 0) return;
            _activated = true;
            if (_repeatability > 1)
            {
                --_repeatability;
            }
            Debug.Log("GameTurnStarted: TBR = " + _currentTurnBeforeReset + ", Rep = " + _repeatability);
        }

        /// <summary>
        /// Indicate if the interaction is valid for the given parameters.
        /// </summary>
        /// <param name="mode">Activation mode.</param>
        /// <param name="from">Tile containing the player.</param>
        /// <param name="target">Tile to interact with.</param>
        /// <returns>If it is interactable.</returns>
        public bool IsInteractable(ActivationMode mode, Vector2 from, Vector2 target)
        {
            // Interactable game object is not activated or wrong activation mode.
            if (!_activated || !_activationMode.Contains(mode)) return false;
            switch (mode)
            {
                case ActivationMode.Interaction:
                    // Can only interact if the tiles are adjacents.
                    if((int)(Mathf.Abs(target.x - from.x) + Mathf.Abs(target.y - from.y)) > 1) return false;
                    // Check possible sides.
                    if (_activationSides.Contains(ActivationSide.All)) return true;
                    else if (from.x < target.x) return _activationSides.Contains(ActivationSide.Left);
                    else if (from.x > target.x) return _activationSides.Contains(ActivationSide.Right);
                    else if (from.y < target.y) return _activationSides.Contains(ActivationSide.Bottom);
                    else if (from.y > target.y) return _activationSides.Contains(ActivationSide.Top);
                    else return false;
                default:
                    // Can only interact if the damages or electroshock are on the same tile as the target.
                    return ((int)target.x == (int)from.x) && ((int)target.y == (int)from.y);
            }
        }

        /// <summary>
        /// Handle interaction with the given tile.
        /// </summary>
        /// <param name="tile">Tile coordinates.</param>
        /// <returns>The result.</returns>
        public InteractionHandler.Result Interact(Vector2 tile)
        {
            if(_handlers == null || _handlers.Count == 0) return InteractionHandler.Result.Remove;
            // Call all the handlers.
            for (var i = 0; i < _handlers.Count; ++i)
            {
                var result = _handlers[i].Handle(tile);
                // Remove the interactable game object.
                if ((result & InteractionHandler.Result.Remove) == InteractionHandler.Result.Remove)
                {
                    Destroy(_handlers[i]);
                    _handlers.RemoveAt(i);
                    --i;
                }
                // Stop the propagation.
                if ((result & InteractionHandler.Result.Stop) == InteractionHandler.Result.Stop)
                {
                    break;
                }
            }
            // Check if empty or no reset or no repeatability.
            if (_handlers.Count == 0 || _turnBeforeReset == NO_RESET || _repeatability == 1) return InteractionHandler.Result.Remove;
            // Deactivate the interactable game object.
            if (_turnBeforeReset != 0)
            {
                _activated = false;
                _currentTurnBeforeReset = _turnBeforeReset;
            }
            else if (_repeatability > 1)
            {
                --_repeatability;
            }
            Debug.Log("Interacted: TBR = " + _currentTurnBeforeReset + ", Rep = " + _repeatability);
            return InteractionHandler.Result.Done;
        }

        public override Color GridEditorColor
        {
            get { return new Color(0, 1, 0, 0.5f); }
        }

        public override void Action(GridManager.Tile tile)
        {
            tile.mInteractable = this;
        }

#if UNITY_EDITOR
        /// <summary>
        /// Draw the arrows in editor.
        /// </summary>
        public void OnDrawGizmosSelected()
        {
            const float dist = 1.0f;
            var all = _activationSides.Contains(ActivationSide.All);
            if (_activationSides.Contains(ActivationSide.Top) || all)
            {
                Gizmos.DrawIcon(transform.position + Vector3.up * dist, "arrow-bottom");
            }
            if (_activationSides.Contains(ActivationSide.Bottom) || all)
            {
                Gizmos.DrawIcon(transform.position + Vector3.down * dist, "arrow-top");
            }
            if (_activationSides.Contains(ActivationSide.Left) || all)
            {
                Gizmos.DrawIcon(transform.position + Vector3.left * dist, "arrow-right");
            }
            if (_activationSides.Contains(ActivationSide.Right) || all)
            {
                Gizmos.DrawIcon(transform.position + Vector3.right * dist, "arrow-left");
            }
        }
#endif
    }
}
