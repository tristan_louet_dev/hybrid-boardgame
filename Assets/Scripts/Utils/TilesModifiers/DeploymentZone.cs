﻿using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Utils.TilesModifiers
{
    public class DeploymentZone : TileModifier {
        public override Color GridEditorColor
        {
            get { return new Color(0.25f, 0.75f, 0.25f, 0.5f); }
        }

        public override void Action(GridManager.Tile tile)
        {
            tile.AllowDeployment = true;
            GridManager.Instance.DeployableTiles.Add(tile);
        }
    }
}
