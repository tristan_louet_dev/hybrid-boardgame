﻿namespace Assets.Scripts.Utils
{
    public enum Orientation
    {
        Left,
        Right,
        Up,
        Down
    }
}
