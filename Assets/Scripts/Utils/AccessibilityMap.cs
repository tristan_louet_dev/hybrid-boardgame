﻿//#define DEBUG

using System.Collections.Generic;
using Assets.commons.Scripts;
using Assets.commons.Scripts.Entity;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class AccessibilityMap : Singleton<AccessibilityMap>
    {
        private int[,] _tilesMovementLeft;
        public List<Vector2> TilesAvailablesHero { get; private set; }
        

        private Vector2 _vraiOrigine;
        private Camp _camp;

        private readonly List<Vector2> _doneList = new List<Vector2>();
        private readonly List<Vector2> _toDoList = new List<Vector2>();

        /// <summary>
        /// Add the given neighbour tile if possible.
        /// </summary>
        /// <param name="list">List to fill.</param>
        /// <param name="origine">Tile.</param>
        /// <param name="dX">X-coordinate of neighbour tile.</param>
        /// <param name="dY">Y-coordinate of neighbour tile.</param>
        private void AddNeighbour(ICollection<Vector2> list, Vector2 origine, int dX, int dY)
        {
            var toMove = new Vector2(origine[0] + dX, origine[1] + dY);
            if (!GridManager.Instance.Contains(toMove) || GridManager.Instance.TileAt(toMove).mIsHole) return;
            // Check if there is a wall.
            if ((dX < 0 && !GridManager.Instance.TileAt(toMove).mComeFromRight) ||
                (dX > 0 && !GridManager.Instance.TileAt(toMove).mComeFromLeft) ||
                (dY < 0 && !GridManager.Instance.TileAt(toMove).mComeFromUp) ||
                (dY > 0 && !GridManager.Instance.TileAt(toMove).mComeFromDown)) return;
            // Check if there is a non-allied unit.
            var unit = GridManager.Instance.UnitAt(toMove);
            if (unit != null && ((_camp & unit.Camp) != unit.Camp)) return;
            list.Add(toMove);
        }
        
        /// <summary>
        /// Get accessible neighbour tiles from the given one.
        /// </summary>
        /// <param name="origine">Tile.</param>
        /// <returns>Neighbour tiles.</returns>
        private List<Vector2> GetTileNeighbour(Vector2 origine)
        {
            var neighbours = new List<Vector2>();
            AddNeighbour(neighbours, origine, -1, 0);
            AddNeighbour(neighbours, origine, 1, 0);
            AddNeighbour(neighbours, origine, 0, -1);
            AddNeighbour(neighbours, origine, 0, 1);
            return neighbours;
        }

        bool IsInLists(Vector2 toCheck)
        {
            return _doneList.Contains(toCheck) ||
                _toDoList.Contains(toCheck);
        }

        /// <summary>
        /// Before using AccessibilityMap you need to use this function
        /// Compute the tile coast from the origine to the mouvement point left given.
        /// </summary>
        /// <param name="origine">origne in tile coordinate</param>
        /// <param name="mouvementPointLeft"></param>
        public void ComputePath(Vector2 origine, int mouvementPointLeft)
        {
            ComputePath(Camp.All, origine, mouvementPointLeft);
        }

        /// <summary>
        /// Before using AccessibilityMap you need to use this function
        /// Compute the tile coast from the origine to the mouvement point left given.
        /// </summary>
        /// <param name="unit">Unit.</param>
        /// <param name="origine">origne in tile coordinate</param>
        /// <param name="mouvementPointLeft"></param>
        public void ComputePath(Unit unit, Vector2 origine, int mouvementPointLeft)
        {
            ComputePath(unit.Camp, origine, mouvementPointLeft);
        }

        /// <summary>
        /// Before using AccessibilityMap you need to use this function
        /// Compute the tile coast from the origine to the mouvement point left given.
        /// </summary>
        /// <param name="camp">Units of this camp can be traversed.</param>
        /// <param name="origine">origne in tile coordinate</param>
        /// <param name="mouvementPointLeft"></param>
        public void ComputePath(Camp camp, Vector2 origine, int mouvementPointLeft)
        {
            Raz();
            if (!GridManager.Instance.Contains(origine) || mouvementPointLeft <= 0)
            {
                return;
            }

            _vraiOrigine = origine;
            _camp = camp;

            UpdateTile(origine, mouvementPointLeft);
            _toDoList.Add(origine);
            InternComputePath();

            ComputeTileAvailable();

        }

        void UpdateTile(Vector2 tile, int mouv)
        {
            _tilesMovementLeft[(int)tile.x, (int)tile.y] = mouv;
#if DEBUG
            _textes[(int)tile.x, (int)tile.y].text = _tilesMovementLeft[(int)tile.x, (int)tile.y] + "";
#endif
        }

        void InternComputePath()
        {
            while (_toDoList.Count > 0)
            {
                // Select best tile
                Vector2 currentTile = -Vector2.one;
                int dist = -1;
                foreach (var t in _toDoList)
                {
                    if (_tilesMovementLeft[(int)t.x, (int)t.y] > dist)
                    {
                        dist = _tilesMovementLeft[(int)t.x, (int)t.y];
                        currentTile = t;
                    }
                }

                int mouvementPointLeft = _tilesMovementLeft[(int)currentTile.x, (int)currentTile.y];

                _doneList.Add(currentTile);
                _toDoList.Remove(currentTile);



                List<Vector2> neibough = GetTileNeighbour(currentTile);



                foreach (var n in neibough)
                {
                    //Debug.Log((int)n.x + ", " + (int)n.y + " " + _tilesMovementLeft[(int)n.x, (int)n.y]);
                    int moveCost = GridManager.Instance.TileAt(n).mCostToComeIn;
                    if (_tilesMovementLeft[(int)n.x, (int)n.y] < (mouvementPointLeft - moveCost))
                    {
                        UpdateTile(n, mouvementPointLeft - moveCost);
                    }
                }

                foreach (var n in neibough)
                {
                    if (!IsInLists(n) &&
                        _tilesMovementLeft[(int)n.x, (int)n.y] > 0)
                        _toDoList.Add(n);
                }


            }


        }



#if DEBUG
        private TextMesh[,] _textes;
#endif

        /// <summary>
        /// Prepare the accessibilityMap to be used any time with ComputePath
        /// </summary>
        public void Start()
        {
            TilesAvailablesHero = new List<Vector2>();
#if DEBUG
            if (_textes == null)
            {
                var parentText = new GameObject("ParentText AccessibilityMap");
                _textes = new TextMesh[GridManager.Instance.Width, GridManager.Instance.Height];

                for (var y = 0; y < GridManager.Instance.Height; ++y)
                {
                    for (var x = 0; x < GridManager.Instance.Width; ++x)
                    {
                        Vector3 t = GridManager.Instance.TileToWorld(new Vector2(x, y));
                        t.y += GridManager.Instance.WorldTileSize.y;

                        GameObject g = Instantiate(Resources.Load("TextTest", typeof(GameObject))) as GameObject;
                        g.transform.parent = parentText.transform;
                        _textes[x, y] = g.GetComponent<TextMesh>();
                        _textes[x, y].text = "";
                        g.transform.position = t;

                    }
                }

            }
#endif
            _tilesMovementLeft = new int[GridManager.Instance.Width, GridManager.Instance.Height];

            for (var y = 0; y < GridManager.Instance.Height; ++y)
            {
                for (var x = 0; x < GridManager.Instance.Width; ++x)
                {
                    // textes[x, y].font = Resources.GetBuiltinResource<Font>("Arial.ttf");
                    Vector3 t = GridManager.Instance.TileToWorld(new Vector2(x, y));
                    t.y += GridManager.Instance.WorldTileSize.y;

                    _tilesMovementLeft[x, y] = -1;
                }
            }
        }

        private void Raz()
        {
            _doneList.Clear();
            _toDoList.Clear();
            TilesAvailablesHero.Clear();

            for (var y = 0; y < GridManager.Instance.Height; ++y)
            {
                for (var x = 0; x < GridManager.Instance.Width; ++x)
                {
                    _tilesMovementLeft[x, y] = -1;
#if DEBUG
                    _textes[x, y].text = _tilesMovementLeft[x, y] + "";
#endif
                }
            }
        }



        /// <summary>
        /// return a list of tiles where the unit can move accordingly to the last ComputePath called
        /// </summary>
        /// <returns></returns>
        private void ComputeTileAvailable()
        {
            TilesAvailablesHero.Clear();
            var tile = new Vector2();
            for (var y = 0; y < GridManager.Instance.Height; ++y)
            {
                for (var x = 0; x < GridManager.Instance.Width; ++x)
                {
                    if (_tilesMovementLeft[x, y] < 0) continue;
                    tile.x = x;
                    tile.y = y;
                    var unit = GridManager.Instance.UnitAt(tile);
                    if (unit == null)
                    {
                        TilesAvailablesHero.Add(tile);
                    }
                }
            }
        }

        /// <summary>
        /// return the shortest path within a list of waypoints from the target to the origine given in the last ComputePath called
        /// CAUTION path contain the origine 
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public List<Vector2> GetPath(Vector2 target)
        {
            Debug.Log("start GetPath");

          /*  if (!TilesAvailablesHero.Contains(target))
            {
                Debug.Log(target + " not available");
                return new List<Vector2>();
            }*/

            int currentMov = _tilesMovementLeft[(int)target.x, (int)target.y];

            if (currentMov == -1)
            {
                Debug.Log("No more movement available");
                return new List<Vector2>();
            }

            List<Vector2> path = new List<Vector2>();

            List<Vector2> sols = new List<Vector2> { target };

            while (sols.Count > 0)
            {
                float dist = float.MaxValue;
                Vector2 theSol = -Vector2.one;

                foreach (var s in sols)
                {
                    if (Vector2.Distance(s, _vraiOrigine) < dist)
                    {
                        theSol = s;
                        dist = Vector2.Distance(s, _vraiOrigine);
                    }
                }

                // Move to the founded solution
                path.Add(theSol);
                Vector2 currentPos = theSol;

                sols.Clear();

                // prepare next possible solutions
                foreach (var n in GetTileNeighbour(currentPos))
                {
                    if (_tilesMovementLeft[(int)n.x, (int)n.y] > currentMov)
                    {
                        sols.Clear();

                        currentMov = _tilesMovementLeft[(int)n.x, (int)n.y];
                        sols.Add(n);
                    }
                    else if (_tilesMovementLeft[(int)n.x, (int)n.y] == currentMov)
                    {
                        sols.Add(n);
                    }

                }
            }

            path.Reverse();
            return path;
        }



    }
}
