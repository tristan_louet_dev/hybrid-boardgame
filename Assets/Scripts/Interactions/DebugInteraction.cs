﻿using Assets.Scripts.Managers;
using Assets.Scripts.Utils.TilesModifiers;
using UnityEngine;

namespace Assets.Scripts.Interactions
{
    /// <summary>
    /// Interaction displaying a predefined message.
    /// </summary>
    public class DebugInteraction : InteractionHandler
    {
        /// <summary>
        /// Message to display.
        /// </summary>
        [SerializeField]
        private string _message;

        /// <summary>
        /// Result to return.
        /// </summary>
        [SerializeField]
        private InteractionHandler.Result _result;

        /// <summary>
        /// Automatically interact with the tile.
        /// </summary>
        [SerializeField]
        private bool _automatic;

        /// <summary>
        /// Activation mode.
        /// </summary>
        [SerializeField]
        private ActivationMode _mode;

        /// <summary>
        /// Tile containing the player.
        /// </summary>
        [SerializeField]
        private Vector2 _from;

        /// <summary>
        /// Tile to interact with.
        /// </summary>
        [SerializeField]
        private Vector2 _target;

        /// <summary>
        /// Handle interaction.
        /// </summary>
        /// <param name="tile">Tile coordinates.</param>
        /// <returns>Result.</returns>
        public override InteractionHandler.Result Handle(Vector2 tile)
        {
            Debug.Log("Interaction (" + (int)tile.x + ", " + (int)tile.y + "): " + _message);
            return _result;
        }

        void Update()
        {
            if (_automatic)
            {
                GridManager.Instance.Interact(_mode, _from, _target);
            }
        }
    } 
}
