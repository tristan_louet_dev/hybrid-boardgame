﻿using Assets.Scripts.Managers;
using Assets.Scripts.Utils.TilesModifiers;
using UnityEngine;

namespace Assets.Scripts.Interactions
{
    /// <summary>
    /// Switch for doors.
    /// </summary>
    public class DoorSwitch : InteractionHandler
    {
        /// <summary>
        /// Door instance.
        /// </summary>
        [SerializeField]
        private GameObject _door;
    
        public override InteractionHandler.Result Handle(Vector2 tile)
        {
            Destroy(_door);
            return InteractionHandler.Result.Remove;
        }

        private void OnDestroy()
        {
            Debug.Log("Map changed");
            EventManager.mOnMapChanged.Fire();
        }
    }
}
