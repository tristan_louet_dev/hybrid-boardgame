﻿using Assets.Scripts.Utils.TilesModifiers;
using Assets.Scripts.Utils.TilesModifiers.Spawners;
using UnityEngine;

namespace Assets.Scripts.Interactions
{
    [RequireComponent(typeof(InteractionSpawner))]
    public class SpawnInteraction : InteractionHandler
    {
        /// <summary>
        /// Spawner instance.
        /// </summary>
        private InteractionSpawner _spawner;

        /// <summary>
        /// Get the spawner.
        /// </summary>
        private void Awake()
        {
            _spawner = GetComponent<InteractionSpawner>();
        }

        /// <summary>
        /// Call the spawner.
        /// </summary>
        /// <param name="tile"></param>
        /// <returns></returns>
        public override Result Handle(Vector2 tile)
        {
            return _spawner.Handle(tile);
        }
    }
}
