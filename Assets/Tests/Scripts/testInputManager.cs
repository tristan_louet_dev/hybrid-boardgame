﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Managers;

public class testInputManager : MonoBehaviour {

    public List<int> test = new List<int>();

	// Use this for initialization
	void Start () {
        EventManager.mOnAddTouchInput += OnAddTouchInput;
        EventManager.mOnRemoveTouchInput += OnRemoveTouchInput;
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    private void OnAddTouchInput(int a)
    {
        //    Debug.Log("add " + a);
        test.Add(a);
    }

    private void OnRemoveTouchInput(int a)
    {
        test.Remove(a);
    }

}
