﻿using Assets.commons.Scripts.Entity;
using Assets.Scripts.Entity;
using Assets.Scripts.Managers;
using Assets.Scripts.Screen;
using Assets.Scripts.Units;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Tests.Scripts
{
    public class TestAccessibilityMap : MonoBehaviour
    {
        public int mMovementPoints;
        public Camp camp;

        Unit target;

        void Start()
        {
            var obj = new GameObject("target");
            if (camp == Camp.Enemy)
            {
                target = obj.AddComponent<RifleGuard>();
            }
            else
            {
                target = obj.AddComponent<ServerPlayer>();
            }
            obj.transform.parent = transform;
            foreach (var p in PlayerManager.Instance.Players)
            {
                p.Tile = GridManager.Instance.WorldToTile(p.transform.position);
            }
        }

        void Update()
        {

            AccessibilityMap.Instance.ComputePath(target, GridManager.Instance.WorldToTile(target.transform.position), mMovementPoints);

            GridDrawer.Instance.Uncolorize(Color.black);
            foreach (var p in PlayerManager.Instance.Players)
            {
                GridDrawer.Instance.Uncolorize(p.Color);
            }


            foreach (var tile in AccessibilityMap.Instance.TilesAvailablesHero)
            {
                GridDrawer.Instance.Colorize(tile, Color.black);
            }
            foreach (var p in PlayerManager.Instance.Players)
            {
                GridDrawer.Instance.Colorize(p.Tile, p.Color);
            }
        }
    }
}