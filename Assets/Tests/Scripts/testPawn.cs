﻿using UnityEngine;
using System.Collections;
using Assets;
using Assets.Scripts.Managers;
using Assets.Scripts.Utils;

public class testPawn : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
	    EventManager.mOnPawnGoOnBoard += OnPawnGoOnBoard;
	    EventManager.mOnPawnGoAway += OnPawnGoAway;
	}

    void OnPawnGoOnBoard(Pawn p)
    {

        Debug.Log(p + " On Board");
        Debug.Log(p.gameObject.transform.position);   
    }

    void OnPawnGoAway(Pawn p)
    {
        Debug.Log(p + " Away");
        Debug.Log(p.gameObject.transform.position);   
    }

}
