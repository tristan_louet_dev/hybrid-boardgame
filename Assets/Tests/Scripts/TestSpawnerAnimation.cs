﻿using System;
using System.Collections;
using Assets.Scripts.Utils.TilesModifiers.Spawners;
using UnityEngine;

namespace Assets.Tests.Scripts
{
    public class TestSpawnerAnimation : SpawnHandler {
        public GameObject cube;

        public override IEnumerator Handle(GameObject value)
        {
            for (var i = 0.0f; i <= 180; ++i)
            {
                cube.transform.rotation = Quaternion.Euler(0, 0, i);
                yield return null;
            }
        }
    }
}
