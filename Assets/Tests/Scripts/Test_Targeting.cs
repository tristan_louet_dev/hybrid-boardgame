﻿using System.Collections.Generic;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Tests.Scripts
{
    public class Test_Targeting : MonoBehaviour
    {
        public Targeting.TargetingMode mTargetingMode;
        //public bool bounceOnDestructibleWalls;
        public int mBounceCount;

        public List<Targeting.Result> mResults = new List<Targeting.Result>();

        public bool activate = false;

        // Use this for initialization
        void Start () {
            Targeting.Instance.ActiveTargeting(gameObject, mTargetingMode, mBounceCount);
        }
	
        // Update is called once per frame
        void Update () {
            if (activate)
            {
                activate = false;
                mResults = Targeting.Instance.HandleResult();
            }


            foreach (var r in mResults)
            {
                Debug.Log(r.mBounce);
                Debug.Log(r.mDistance);
                Debug.Log(r.mHitted);
            }
        }
    }
}
