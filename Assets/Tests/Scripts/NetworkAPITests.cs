﻿using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.GameStates;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Tests.Scripts
{
    public class NetworkAPITests : MonoBehaviour {
        void Update () {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                NetworkManager.Instance.NetworkAPI.UpdateGameState(GameState.PlayerTurn, Character.Amy);
            }
        }
    }
}
